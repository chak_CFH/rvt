/**
 * Created by Vaseker on 19.02.14.
 */
'use strict';
//убрать выделение текста, вызванное много-кликом
function clearSelection() {
    if (window.getSelection) {
        if (window.getSelection().empty) {  // Chrome
            window.getSelection().empty();
        } else if (window.getSelection().removeAllRanges) {  // Firefox
            window.getSelection().removeAllRanges();
        }
    } else if (document.selection) {  // IE?
        document.selection.empty();
    }
}
//Код ниже - галимая самопись. Лапшекод. Индокод. Возьмите ложку, вставьте в глаз, надавите.
var ZoomInstance = function (img) {
    var self = this;
    img = $(img);

    this.kill = false;
    this.img = img;
    this.full = img.data('original');
    this.small = img.attr('src');

    this.container = this.img.parent('.photo-container');

    this.preloads = $('.preloads');
    if (this.preloads.length == 0) {
        this.preloads = $('<div>', {class: 'preloads'});
        this.preloads.appendTo($('body'));
    }

    this.imgsPreload({
        done: this.init
    });

    return this;
};
ZoomInstance.prototype.imgsPreload = function (opt) {
    opt = opt || {};
    var self = this,
        total = 2,
        done = function () {
            return function () {
                if (--total == 0) {
                    if (opt.done) opt.done.call(self);
                }
            }
        };
    this.small = $('<img>', {src: this.small});
    this.full = $('<img>', {src: this.full});
    this.small.on('load', done(this.small));
    this.full.on('load', done(this.full));
    this.small.appendTo(this.preloads);
    this.full.appendTo(this.preloads);
};
ZoomInstance.prototype.calcDimensions = function () {
    if (this.Hzoom && this.Hzoom > 0) {
        return;
    }

    this.Wsmall = this.small.width();
    this.Hsmall = this.small.height();
    this.Wfull = this.full.width();
    this.Hfull = this.full.height();
    this.borderWidths = 3;
    this.Wzoom = this.container.width() - this.borderWidths;
    this.Hzoom = this.container.height() - this.borderWidths;
    this.diff = this.Wsmall > this.Hsmall ?
        this.Wfull / this.Wsmall :
        this.Hfull / this.Hsmall;
    this.Wloupe = Math.floor(this.Wsmall / this.diff);
    this.Hloupe = Math.floor(this.Hsmall / this.diff);

    this.WZloupe = Math.floor(this.Wloupe / 2);
    this.HZloupe = Math.floor(this.Hloupe / 2);

    var position = this.img.position();

    this.Xmin = position.left;
    this.Ymin = position.top;

    this.Xmax = this.Wsmall - this.Wloupe - this.borderWidths + this.Xmin;
    this.Ymax = this.Hsmall - this.Hloupe - this.borderWidths + this.Ymin;

    this.ZoomLeft = this.container.width() + 20;
};
ZoomInstance.prototype.calcPositions = function (e) {
    var x = e.offsetX,
        y = e.offsetY,
        _x = (Math.min(this.Xmax, Math.max(this.Xmin, x - this.WZloupe))),
        _y = (Math.min(this.Ymax, Math.max(this.Ymin, y - this.HZloupe)));
    this.zoom.ctl.css({
        left: _x,
        top: _y
    });
    this.full.css({
        marginLeft: -(_x - this.Xmin) * this.diff,
        marginTop: -(_y - this.Ymin) * this.diff
    });
    e.preventDefault();
};
ZoomInstance.prototype.init = function () {
    var self = this;
    this.calcDimensions();
    if (self.Wfull > self.Wsmall.width || self.Hfull > self.Hsmall) {
        this.img.on('mouseover', function (e) {
            self.in(e);
        })
    }
};
ZoomInstance.prototype.in = function (e) {
    var self = this;
    this.calcDimensions();
    if (!this.zoom) {
        this.zoom = {
            layer: $('<div>', {
                css: {
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                },
                on: {
                    mousemove: function (e) {
                        self.move(e);
                    },
                    mouseleave: function (e) {
                        self.out(e);
                    }
                }
            }),
            ctl: $('<div>', {
                class: 'zoom-ctl',
                css: {
                    width: this.Wloupe,
                    height: this.Hloupe
                }
            }),
            win: $('<div>', {
                class: 'zoom-win',
                css: {
                    left: this.ZoomLeft,
                    height: this.Hzoom,
                    width: this.Wzoom
                }
            })
        };
        this.zoom.win.append(this.full);
    }
    this.img.after(this.zoom.ctl, this.zoom.layer, this.zoom.win);
    this.calcPositions(e);
};
ZoomInstance.prototype.out = function () {
    this.zoom = this.zoom || {};
    if (this.kill) {
        this.zoom.ctl.remove();
        this.zoom.layer.remove();
        this.zoom.win.remove();
    } else {
        this.zoom.ctl.detach();
        this.zoom.layer.detach();
        this.zoom.win.detach();
    }
};
ZoomInstance.prototype.move = function (e) {
    if (!this.zoom) {
        return this.in(e);
    }
    this.calcPositions(e);
};
var Zoom = function (obj) {
    var self = this;
    this.zooms = [];
    if (obj.length > 0) {
        obj.each(function (i, img) {
            self.zooms.push(new ZoomInstance($(img)));
        });
    }
    return this;
};
//модальки
var modal = function (block, param) {//типа класс
    this.layer = $('<div>', {class: 'modal'});
    if (param && param.css) {//установка css
        this.layer.css(param.css);
    }
    if (param && param.after) {//определение относительной позиции модальки - мульти-модальки
        param.after.after(this.layer);
    } else this.layer.prependTo($('.wrapper:first'));
    if (param && param.kill) {//удалять модальку насовсем при закрытии
        this.toKill = true;
    }
    this.window = $(window);
    if (block) {
        this.pop(block);
    }
};
modal.prototype.pop = function (block, cb) {//показать модальку
    var $this = this;
    if ($this.layer.hasClass('visible')) {//если уже открыта другая модалька
        return new modal(block, {//надо сделать новую
            css: {
                zIndex: parseInt($this.layer.css('z-index')) + 1//немного выше
            },
            after: $this.layer,//немного позже
            kill: true//и убить при закрытии
        });
    }
    if (this.block)this.block.detach();//снос предыдущего блока в модальке
    this.closeButton = $('<div>', {class: 'close', text: 'x'});
    var $block = $(block);
    this.block = $('<div>', {//враппер контента попапа
        class: 'inner',//можно даже стиль нарисовать
        html: function () {
            return $block.prepend($this.closeButton);
        }
    });
    this.block.appendTo(this.layer);
    this.closeButton.add(this.block.find('.close-modal')).on('click', function (e) {
        var t = $(e.target);
        if (t.hasClass('close-modal')) {
            var target = t.data('target');
            if (!$block.hasClass(target)) return;
        }
        return $this.close.call($this);
    });
    $this.layer.addClass('visible');//показать
    setTimeout(function () {//dom родил
        var wH = $this.window.height() / 2,//высота вьюпорта
            bH = $this.block.height() / 2;//высота модальки
        $this.layer.css('padding-top', $this.window.scrollTop() + Math.max(0, Math.floor(wH - bH)));//отступ модальку сверху от вьюпорта
        $this.layer.on('click', function (e) {//хэндлер клика по затемнению
            if ($(e.target).hasClass('modal')) $this.close();//закрыть модальку
        });
        return cb ? cb($this.block) : null;//вызывать каллбэк или нет
    }, 0);
};
modal.prototype.close = function () {//закрытие модальки
    if (this.block)this.block.detach();//отвязка от dom
    this.layer.removeClass('visible');//скрытие
    this.layer.off('click');//отмена хэндлеров
    if (this.toKill)this.kill();//убить, если надо
};
modal.prototype.kill = function () {//убийца модальки
    this.layer.remove();
    delete this;
};
var Modal = new modal();

//корзина
var cart = function () {
    this.cookieName = 'cart';
    this.stores = ['items', 'graffiti'];
    this.store = this.stores[0];
    this.data = $.cookie(this.cookieName) || {//кука
        items: [],//элементы (товары)
        graffiti: []//элементы (граффити)
    };//куки корзины
    this.mini = $('#cart-mini');//корзина в юзер-баре
    this.miniDeafultText = this.mini.text();//текст в корзине по умолчанию (без товаров)
    this.initCartPage();//инициализация страницы корзины
    this.setStore();
    this.miniUpdate();
}, Cart;

cart.prototype.initCartPage = function () {//инициализация страницы корзины
    //страница со списком товаров в корзине
    var $class = this,
        cartDetailed = $('.cart-details');//привязка на присутствие класса в документе
    if (cartDetailed.length > 0) {
        var items = cartDetailed.find('.item-item'),//товары
            graffiti = cartDetailed.find('.item-graffiti');//баллоны

        graffiti.each(function (i, g) {//граффити
            g = $(g);
            var id = g.data('id'),
                $amount = g.find('.amount'),
                $sum = $(g.find('.total-graffiti-price')),
                ballons = 0,
                cost = 0;

            $class.data.graffiti.forEach(function (v) {
                if (v.id == id) {
                    ballons += v.amount;
                    cost = parseFloat($(g.find('.cost')).text().replace(/[^\d]*/im, ''));
                }
            });

            $amount.text(ballons);
            $sum.text(ballons * cost);

            g.find('.tool-remove').on('click', function () {//удалить
                Cart.remove({
                    from: 'graffiti',
                    id: g.data('id')
                }, function () {
                    g.remove();
                });
            });
        });

        items.each(function (i, g) {//НЕ граффити
            g = $(g);
            var $amount = g.find('.amount'),
                $sum = $(g.find('.total-item-price')),
                $price = parseFloat($(g.find('.cost')).text().replace(/[^\d]*/im, '')),
                find = {//данные о товаре в куках
                    from: 'items',
                    id: g.data('id'),
                    spec: g.data('spec'),
                    size: g.data('size')
                };

            $class.get(find, function (cData) {
                if (cData !== null) {
                    $amount.val(cData.amount);
                    $class.itemUpdate(cData.amount, $price, $sum);
                    $amount.on('change', function () {
                        var $t = $(this),
                            $a = parseInt($t.val());
                        if (!isNaN($a)) {
                            if ($a !== cData.amount) {
                                $class.update(find, {
                                    amount: $a
                                }, function () {
                                    cData.amount = $a;
                                    $class.itemUpdate($a, $price, $sum);
                                });
                            }
                        }
                    });
                    g.find('.tool-remove').on('click', function () {//удалить
                        $class.remove({
                            from: 'items',
                            id: g.data('id'),
                            spec: g.data('spec')
                        }, function () {
                            g.remove();
                        });
                    });
                }
            });
        });

        cartDetailed.data('info', $(cartDetailed.find('.cart-delivery .info')));
        var free = $('<div>', {
            class: 'text',
            html: 'Бесплатная доставка'
        });
        free.appendTo(cartDetailed.data('info')).hide();
        cartDetailed.data('infoFree', free);
        cartDetailed.data('total', $(cartDetailed.find('.cart-total .value')));
        cartDetailed.data('deliveryNeed', parseInt($(cartDetailed.find('.cart-delivery .max .val')).text().replace(/[^\d]/ig, '')));
        cartDetailed.data('deliveryDiff', $(cartDetailed.find('.cart-delivery .need .val')));
        $class.details = cartDetailed;
        $class.updateDetails();
    }
};
cart.prototype.itemUpdate = function (amount, cost, sum) {//обновить элемент в детальной корзине
    sum.text(amount * cost);
};
cart.prototype.setStore = function (storage) {
    if (storage && this.stores.indexOf(storage) > 0) {
        this.store = storage;
    }
    return this.items = this.data[this.store];
};
cart.prototype.parse = function (item) {//подготовка объекта, приведение типов
    if (item.id)item.id = parseInt(item.id);
    if (item.amount)item.amount = parseInt(item.amount);
    if (item.price)item.price = parseFloat(item.price.replace(/[^\d\,\.]/igm, '').replace(/\,/gm, ''));
    return item;
};
cart.prototype.add = function (data) {//добавить элемент в корзину
    var $cart = this,
        item = $cart.parse(data.obj),
        pfx = data.graffiti ? 'graffiti' : 'items';
    $cart.setStore(pfx);
    var exists = $cart.find(item);//поиск этого товара в корзине
    if (exists === null) {//не нашли
        if (item.amount > 0) $cart.items.push(item);//добавили
    } else {//нашли
        if (item.amount > 0)
            $cart.items[exists.index] = item;//заменили
        else {
            $cart.items.splice(exists.index, 1);//удалили из корзины, если у товара количество 0
        }
    }
    $cart.save();//сохранить корзину
    if (data.button) {
        data.button.text('добавлено в корзину');
        data.button.off('click').on('click', function () {
            return window.location.href = '/cart/';
        });
    }
};

cart.prototype.save = function (cb) {//сохранить корзину в куках
    $.cookie(this.cookieName, this.data);
    this.updateDetails();
    this.miniUpdate(cb);
};

cart.prototype.miniUpdate = function (cb) {//обновляет текст в корзине на панели пользователя
    var $cart = this,
        total = 0;
    this.stores.forEach(function (store) {
        total += $cart.data[store].length;
    });
    if (total > 0) {
        this.mini.text('В корзине ' + total + ' товаров');
    } else {
        this.mini.text(this.miniDeafultText);
    }
    return cb ? cb() : null;
};

cart.prototype.find = function (item) {//найти элемент в корзине
    var $cart = this,
        items = $cart.items,
        ret = null;
    if (items === undefined || items === null)return null;
    items.forEach(function (row, i) {
        if (item.id == row.id && item.size == row.size && item.color == row.color) {
            ret = {item: row, index: i};
        }
    });
    return ret;
};

cart.prototype.remove = function (data, cb) {//удаление элемента из корзины
    var $class = this,
        $r = [];//список без удаленного
    $class.data[data.from].forEach(function (v, k) {//смотрим все добавленное
        var rm = false;//пометка "удалить запись"
        if (v.id == data.id) {//если элемент найден
            if (data.spec !== undefined) {//передан ключ "spec"
                if (v.spec == data.spec) {
                    rm = true;//удаляем его
                }
            } else {
                rm = true;//удаляем его
            }
        }
        if (!rm)$r.push(v);
    });
    $class.data[data.from] = $r;
    $class.save(cb);
};

cart.prototype.updateDetails = function () {//обновить инфу в детальной корзине
    var $class = this,
        $continue = $('.continue.button'),
        empty = true;
    if ($class.details){
        $class.totalCost();
        $class.stores.forEach(function (store) {
            if (empty && $class.data[store] && $class.data[store].length) {
                empty = false;
            }
        });
    }

    if (empty)
        $continue.addClass('hidden');
    else
        $continue.removeClass('hidden');
};

cart.prototype.totalCost = function () {//итого в деньгах
    var $class = this,
        total = 0;
    $class.data.items.forEach(function (v) {
        total += v.amount * v.price;
    });
    var gIds = [];//ID просчитанных графити-товаров
    $class.data.graffiti.forEach(function (v) {
        if (gIds.indexOf(v.id) < 0) {
            //gIds.push(v.id);
            total += v.amount * v.price;
        }
    });
    $class.details.data('total').text(total);
    var diff = $class.details.data('deliveryNeed') - total;
    if (diff <= 0) {
        $class.details.data('infoFree').show().siblings().hide();
    } else {
        $class.details.data('infoFree').hide().siblings().show();
    }
    $class.details.data('deliveryDiff').text(diff);
};

cart.prototype.update = function (find, v, cb) {//обновить инфу о товаре  корзине
    var $class = this;
    $class.get(find, function (data, k) {
        if (data) {
            $class.data[find.from][k].amount = v.amount;
            $class.save(cb);
        }
    });
};

cart.prototype.get = function (data, cb) {//получение инфы о товаре из корзины
    var $class = this,
        $r = false;
    $class.data[data.from].forEach(function (v, k) {//смотрим все добавленное
        var it = false;//пометка "этот"
        if (v.id == data.id) {//если элемент найден
            if (data.spec !== undefined) {//передан ключ "spec"
                if (v.spec == data.spec) {
                    if (data.size !== undefined) {
                        if (v.size == data.size) {
                            it = true;//отдаем его
                        }
                    } else {
                        it = true;//отдаем его
                    }
                }
            } else {
                it = true;//отдаем его
            }
        }
        if (it) {
            $r = true;
            return cb(v, k);
        }
    });
    if (!$r)return cb(null);
};

//galleryViewer
var galleryViewer = function (gallery) {
    var $class = this;
    if (gallery && gallery.length > 0) {
        $class.gallery = gallery;//запоминаем блок галерейки
        $class.fullSizes = gallery.find('.size-full .photo');//запоминаем список всех больших фоток
        $class.thumbs = gallery.find('.thumbnails .thumb');//запоминаем список всех thumbnails
        $class.current = null;//выбранное изображение
        $class.total = 0;//всего картинок
        if ($class.thumbs.length > 0 && $class.thumbs.length == $class.fullSizes.length) {//если thumbs есть
            $class.thumbs.each(function (i, thumb) {
                var $thumb = $(thumb);
                $class.total++;
                $thumb.on('click', function () {
                    $class.choose(i);
                });
            });
            $class.choose(0, function () {
                $class.gallery.addClass('inited');
            });
            $class.initControls();//нарисовать кнопки - переключалки вперед/назад
        }
    }
    $class.zoom = new Zoom($class.fullSizes);
    return this;
};
galleryViewer.prototype.initControls = function () {//меняем код-стайл :)
    var self = this,
        next = $('<div>', {
            class: 'control next',
            text: '>',
            on: {
                click: function () {
                    self.next.call(self);
                }
            }
        }),
        prev = $('<div>', {
            class: 'control prev',
            text: '<',
            on: {
                click: function () {
                    self.prev.call(self);
                }
            }
        });
    this.gallery.append(next, prev);
};
galleryViewer.prototype.next = function (e) {
    if (e) e.preventDefault();
    clearSelection();
    var i = this.current + 1;
    if (i >= this.total) i = 0;
    return this.choose(i);
};
galleryViewer.prototype.prev = function (e) {
    if (e) e.preventDefault();
    clearSelection();
    var i = this.current - 1;
    if (i < 0) i = this.total - 1;
    return this.choose(i);
};
galleryViewer.prototype.choose = function (i, cb) {
    clearSelection();
    var $class = this;
    if ($class.current !== i) {
        if ($class.current !== null) {
            $($class.fullSizes[$class.current]).removeClass('this');
            $($class.thumbs[$class.current]).removeClass('this');
        }
        $class.current = i;
        $($class.fullSizes[$class.current]).addClass('this');
        $($class.thumbs[$class.current]).addClass('this');
    }
    return cb ? cb() : null;
};
//wrapper для переключения цветов товара
var itemWrapper = function (item) {
    var $item = $(item),//товар
        $class = this;//класс-обертка
    $class.item = $item;
    $class.id = parseInt($item.data('id'));
    $class.colors = [];//цвета товара
    $class.colorTriggers = {};//кнопки цвета товара
    $class.amount = $($item.find('.amount'));//количество товара для корзины
    $class.price = $($item.find('.price .cost')).text();//цена товара
    $class.prices = $($item.find('.prices'));//ценники товара
    $class.classes = {
        init: 'not-inited',//класс, отмечающий инициализацию
        active: 'this',//выбранный цвет
        graffiti: 'graffiti'//графити-детектор
    };
    $class.graffiti = false;
    if ($item.hasClass($class.classes.init)) {//если товар еще не обработан
        if ($item.hasClass($class.classes.graffiti)) {//обработка страницы граффити
            $class.graffiti = true;
            $class.colorSet = {colors: {}, total: 0, costs: 0};
            $class.colorChooser = $item.find('.color-chooser');
            $class.colorChoosedSet = $item.find('.choosed-colors-set');
            $class.warn = $item.find('.warn');
            var colors = $class.colorChooser.find('.color');
            $class.colorsStore = {};
            colors.each(function (i, color) {//инициализация контроллеров на количестве цвета краски
                $class.graffitiInitControls($(color));
            });
            var colorPopToggler = $item.find('.choose-color').last();//кнопка вызова попапа с выбором цветов
            colorPopToggler.on('click', function () {
                clearSelection();
                Modal.pop($class.colorChooser.show());
            });
            var colorsTotalBlock = $item.find('.graffiti-chooser');//блок с выбором цветов баллона
            colorsTotalBlock.data('total', $(colorsTotalBlock.find('.total')));//счетчик в блоке
            colorsTotalBlock.data('cost', $(colorsTotalBlock.find('.cost-total')));//сумма
            $class.colorsTotalBlock = colorsTotalBlock;
        } else {
            var colors = $item.find('.colors .color');//находим цвета
            colors.each(function (i, color) {
                color = $(color);
                var code = color.data('code');//hex цвета
                $class.colors.push(code);//собираем коды
                color.data('sizes', $($item.find('.sizes .size[data-color="' + code + '"]')));//собираем размеры
                color.data('photos', $($item.find('.photos-color[data-color="' + code + '"]')));//собираем фотки
                //TODO: для избежания тормозов, при $class.hasClass('mini'), вешать ожидание клика на родительский контейнер
                color.on('click', $class.colorChoose(color, code));//вешаем событие клика
                color.data('gallery', new galleryViewer(color.data('photos')));//создаем галлерейку
                $class.colorTriggers[code] = color;//собираем триггеры
            });
        }
        var quick = $item.find('.quick-view');
        if (quick.length > 0) {
            quick = $(quick);
            quick.on('click', function (e) {
                e.preventDefault();
                clearSelection();
                var url = $($item.find('.url')).attr('href');
                $.ajax({
                    method: 'get',
                    url: url + 'quick/',
                    success: function (html) {
                        Modal.pop(html, function (block) {
                            new itemWrapper(block.find('.not-inited'));
                        });
                    }
                });
            });
        }
        $class.wrapperLoaded();//завершаем обработку товара
    }
    return this;
};
itemWrapper.prototype.graffitiGetColor = function (color) {
    return this.colorSet.colors[color.data('spec')];
};
itemWrapper.prototype.graffitiInitColor = function (color) {
    this.colorSet.colors[color.data('spec')] = this.graffitiGetColor(color) || {
        code: color.data('code'),
        name: color.data('name'),
        amount: 0,
        size: color.data('size'),
        spec: color.data('spec')//ID спецификации
    };
    return this.graffitiGetColor(color);
};
itemWrapper.prototype.graffitiSetColor = function (color, data) {
    var $class = this;
    if (color.data['spec'] !== undefined)//непонятный баг с пустой data. надо трейсить
        $class.colorSet.colors[color.data['spec']] = data;
    var total = 0;
    for (var _color in $class.colorSet.colors) {
        total += $class.colorSet.colors[_color].amount;
    }
    $class.colorSet.total = total;
    $class.colorSet.costs = total * $class.price;
    return this.graffitiAmountRender(color);
};
itemWrapper.prototype.graffitiInitControls = function (color) {//навеска эвентов на краску (+ -)
    var $class = this;
    color.data('amount', color.find('.amount'));
    $class.graffitiInitColor(color);
    color.find('.minus').on('click', function (e) {
        clearSelection();
        return $class.graffitiMinus(color, e);
    });
    color.find('.plus').on('click', function (e) {
        clearSelection();
        return $class.graffitiPlus(color, e);
    });
    color.data('amount').on('keyup', function (e) {
        return $class.graffitiInput(color, e);
    });
    $class.colorsStore[color.data('spec')] = color;
};
itemWrapper.prototype.graffitiMinus = function (color, e) {//отнимаем
    var $class = this,
        data = $class.graffitiGetColor(color);
    data.amount = Math.max(0, --data.amount);
    $class.graffitiSetColor(color, data);//сохраняем
    $class.graffitiAmountRender(color);
};
itemWrapper.prototype.graffitiPlus = function (color, e) {//добавляем
    var $class = this,
        data = $class.graffitiGetColor(color);
    ++data.amount;
    $class.graffitiSetColor(color, data);//сохраняем
};
itemWrapper.prototype.graffitiInput = function (color, e) {//ввод количества краски с клавиатуры
    var $class = this,
        amount = parseInt(color.data('amount').val().replace(/[^\d]/ig, '')),
        data = $class.graffitiGetColor(color);
    if (isNaN(amount))return;//введено не число
    data.amount = amount;
    return $class.graffitiSetColor(color, data);
};
itemWrapper.prototype.graffitiAmountRender = function (color) {
    var $class = this,
        data = $class.graffitiGetColor(color);
    if ($class.colorSet.total > 0) {
        $class.colorsTotalBlock.addClass('not-null');
        $class.cartButton.removeAttr('style');
        $class.prices.hide();
        $class.warn.hide();
    } else {
        $class.colorsTotalBlock.removeClass('not-null');
        $class.cartButton.hide();
        $class.prices.show();
        $class.warn.show();
    }
    $class.colorsTotalBlock.data('total').text($class.colorSet.total);
    $class.colorsTotalBlock.data('cost').text($class.colorSet.costs);
    $class.helperColors();
    return color.data('amount').val(data.amount) || color.data('amount').text(data.amount);
};
itemWrapper.prototype.buildHelperColor = function (id, colors) {
    var $class = this,
        color = colors[id];
    if (color.amount) {
        var bindColor = $class.colorsStore[id],
            ctl = $('<div>',{
                class: 'color-mini-controls'
            }),
            amount = $('<span>', {
                text: color.amount
            }),
            $color = $('<div>', {
                class: 'colors',
                html: function () {
                    return $('<div>', {
                        class: 'color this',
                        title: color.name,
                        html: function () {
                            return $('<span>', {
                                style: 'background-color: '+color.code
                            });
                        }
                    });
                }
            }),
            plus = $('<div>', {
                class: 'calc plus',
                text: '+',
                on: {
                    click: function (e) {
                        clearSelection();
                        return $class.graffitiPlus(bindColor, e);
                    }
                }
            }),
            minus = $('<div>', {
                class: 'calc minus',
                text: '-',
                on: {
                    click: function (e) {
                        clearSelection();
                        return $class.graffitiMinus(bindColor, e);
                    }
                }
            });
        ctl.append($color, minus, amount, plus).appendTo($class.colorChoosedSet);
    }
};
itemWrapper.prototype.helperColors = function () {
    var $class = this,
        colors = this.colorSet.colors;
    $class.colorChoosedSet.empty();
    for (var id in colors) {
        $class.buildHelperColor(id, colors);
    }
};
itemWrapper.prototype.wrapperLoaded = function () {//обработка завершения загрузки
    var $class = this;

    if ($class.colors.length > 0) {//если у товара есть цвета

        var colorHash = 'cCi.' + $class.id,
            cachedColor = localStorage ? localStorage.getItem(colorHash) : false;

        var firstColorCode = cachedColor || $class.colors[0],
            firstColorTrigger = $class.getColorTrigger(firstColorCode);
        $class.colorChoose(firstColorTrigger, firstColorCode)();//выбираем первый цвет
        $class.item.removeClass($class.classes.init);//убираем класс "не загружен" с товара
    }
    //если у товара нет цветов, то, благодаря css, будут отображаться первые контейнеры размеров и изображений
    $class.cartInit();//инициализировать корзину для этого товара
};
itemWrapper.prototype.cartInit = function () {
    var $class = this,
        cart = $($class.item.find('.cart-add'));
    if (cart.length > 0) {
        $class.cartButton = cart;
        //IMPROVE: для избежания тормозов, при $class.hasClass('mini'), вешать ожидание клика на родительский контейнер (хотя в списке товаров нет кнопки "в корзину")
        cart.on('click', $class.cartAdd());
        if ($class.graffiti) {
            cart.hide();
            $class.warn.show();
        }
    }
};
itemWrapper.prototype.cartAdd = function () {//добавление в корзину
    var $class = this;
    return function () {
        if ($class.graffiti) {
            var gr = $class.colorSet.colors,
                obj = {
                    id: $class.id,
                    amount: 0,
                    price: $class.price,
                    specs: []
                };
            for (var color in gr) {
                var $c = gr[color];
                if ($c.amount) {
                    obj.specs.push({
                        id: $class.id,
                        spec: $c.spec,
                        size: $c.size,//объем баллона
                        amount: $c.amount,
                        price: $class.price,
                        color: $c.name
                    });
                    obj.amount += $c.amount;
                }
            }
            return Cart.add({obj: obj, button: $class.cartButton, graffiti: true});
        }
        else {
            var $color = $class.getCurrentTrigger(),//триггер цвета
                obj = {
                    id: $class.id,
                    spec: $color.data('spec'),
                    color: $color.attr('title'),
                    size: $color.data('sizes').val(),//размер
                    amount: $class.amount.val(),//количество
                    price: $class.price//цена
                };
            return Cart.add({obj: obj, button: $class.cartButton});
        }
    };
};
itemWrapper.prototype.getColorTrigger = function (code) {//получить триггер цвета по коду
    return this.colorTriggers[code];
};
itemWrapper.prototype.getCurrentTrigger = function () {//получить текщий триггер цвета
    return this.colorTriggers[this.code];
};
itemWrapper.prototype.colorChoose = function (color, code) {//выбор цвета @color=кнопка цвета, @code=HEX цвета
    var $class = this;
    return function (e) {
        if (e)e.preventDefault();//предотвращаем переход по a href
        clearSelection();
        if (!color) {
            return;
        }
        if ($class.code !== code) {//если выбираем другой цвет
            if ($class.code !== undefined) {//если предыдущий цвет определен
                //убираем выделение/видимость объектов предыдущего цвета
                var prev = $class.getCurrentTrigger();
                prev.removeClass($class.classes.active);
                prev.data('sizes').removeClass($class.classes.active);
                prev.data('photos').removeClass($class.classes.active);
            }
            $class.code = code;//сохраняем новый цвет
            localStorage ? localStorage.setItem('cCi.' + $class.id, code) : null;
            //добавляем выделение/видимость объектов цвета
            color.addClass($class.classes.active);
            color.data('sizes').addClass($class.classes.active);
            color.data('photos').addClass($class.classes.active);
        }
        return false;
    };
};
$(function () {
    //конфигурируем плагин куков
    $.cookie.json = true;
    $.cookie.defaults.path = '/';
    $.cookie.defaults.expires = 365;
    //инициализация корзины
    Cart = new cart();
    //слайдеры
    //можно было сделать jQuery-плагин, но ну его нафиг
    //переменные слабоизолированы, возможны конфликты с новым кодом. внимательно!
    //ахтунг! фон для слайдера абсолютно спозиционирован относительно body, поэтому родители bg не должны быть явно позиционированы, чтобы не перехватить контекст
    $('.slider').each(function (i, slider) {
        var $slider = $(slider),//оборачиваем слайдер в jQuery
            $controller = $('<div>', {class: 'controller'}),//контейнер для контроллеров переключения
            $controllerList = $('<div>', {class: 'list'}),//список переключателей
            $timer,//таймер для автопереключения слайдов
        //полноценного слайда, по сути, нет. есть фон и текст, которые объединены по индексу. именно этот индекс отвечает за видимость слайдов. неудобно, однако не должно быть конфликтов при отключенном js + не надо писать фоновую загрузку изображений
            __bgs,//слайды-фоны
            __tls,//слайды-тексты
            __ctls,//переключатели
            __total = 0,//всего слайдов
            __this,//текущий слайд
            navigate = function (i) {//функция переключения
                if (i !== __this) {//если тыкаем в неактивный слайд
                    if (i >= __total)i = 0;//движение только вперед. при выходе за границу справа, обнуляем
                    __this = i;
                    //далее нет проверки на существование объектов (правильность индекса не проверяется)
                    //выбираем нужные элементы слайда: фон, текст, контроллер
                    var $slide = $(__bgs[__this]),
                        $title = $(__tls[__this]),
                        $slideCtl = $(__ctls[__this]);
                    [$slide, $title, $slideCtl].forEach(function (el) {//для всех элементов выполняем одинаковое действие
                        el.addClass('this').siblings().removeClass('this');//новый слайд выбираем, остальные убираем
                    })
                }
            };
        $controllerList.appendTo($controller);//оборачиваем переключатели в контейнер
        //закончено объявление переменных, пошла логика
        $slider.find('.bg').each(function (j) {//строим список переключателей
            ++__total;//увеличиваем "всего" слайдов
            $controllerList.append($('<div>', {//добавляем переключатель
                class: 'slide',
                click: function () {//не стал заморачиваться с уменьшением количества эвентов
                    clearInterval($timer);//пользовательский переход по слайду, отменяет авто-переключение
                    navigate(j);//переход
                }
            }))
        });
        if (__total > 1) {//если слайдов много
            $controller.appendTo($slider);//показываем переключатель
            $timer = setInterval(function () {//запускаем авто-переключатель
                navigate(__this + 1);//просто двигаем к следующему элементу
            }, 5000);//таймер для автопереключения слайдов
        }
        __bgs = $slider.find('.bg');//слайды-фоны
        __tls = $slider.find('.title');//слайды-тексты
        __ctls = $controllerList.find('.slide');//переключатели
        navigate(0);//переключение на 1й слайд
        $slider.addClass('loaded');//навешиваем на слайдер класс для обозначения окончания обработки слайдера
    });
    //!конец слайдера
    var $window = $(window),//оборачиваем window в jquery
        $fixedMenuTransparent = $('.top-menu');//привязываемся к меню разделов;
    //скрипты для главной страницы
    var pageMain = $('html.page-main');//главная страница, цепляемся к классу тега html
    if (pageMain.length > 0) {//если на главной
        var scrollFn;
        $window.on('scroll', function () {//при скролле страницы
            clearTimeout(scrollFn);
            scrollFn = setTimeout(function () {
                if ($window.scrollTop() > 400) {//проверяем, проскроллен ли слайдер
                    $fixedMenuTransparent.addClass('scrolled');//если проскроллен, то делаем у меню непрозрачный фон
                } else {
                    $fixedMenuTransparent.removeClass('scrolled');//иначе оставляем у меня прозрачный фон
                }
            }, 30);//scroll-steps (actually for osx-like smooth scrolling)
        });
    } else {//на другой странице
        $fixedMenuTransparent.addClass('scrolled');//на остальных страницах меню должно быть в состоянии "с фоном, тенью"
        var categories = $('.categories-list');
        if (categories.length > 0) {
            categories.addClass('toggable');
            categories.on('click', function (e) {
                var $o = $(e.target),
                    $s = $o.parents('.section');
                clearSelection();
                if ($s.length > 0) {
                    if ($o.hasClass('title')) {//клик по заголовку
                        if (!categories.hasClass('categories-expanded')) {
                            categories.addClass('categories-expanded');
                        }
                        if (!$s.hasClass('this')) {
                            if (categories.data('current') !== undefined) {
                                categories.data('current').removeClass('this');
                            }
                            categories.data('current', $s);
                            categories.data('current').addClass('this');
                        } else {
                            categories.removeClass('categories-expanded');
                        }
                    }
                }
            });
        }
    }
    var scrollXFn,
        resizeFn,
        scrollXMax;
    $window.on('resize', function () {
        clearTimeout(resizeFn);
        resizeFn = setTimeout(function () {
            scrollXMax = $fixedMenuTransparent.find('.block').first().width() - $window.width();
        }, 0);
    });
    $window.on('scroll', function () {
        clearTimeout(scrollXFn);
        scrollXFn = setTimeout(function () {
            $fixedMenuTransparent.css({
                left: -Math.max(Math.min($window.scrollLeft(), scrollXMax), 0)
            });
        }, 1);
    });
    $window.trigger('resize');
    $window.trigger('scroll');
    $('.toggler-trigger').each(function (i, trigger) {
        var $trigger = $(trigger),
            $target = $($trigger.attr('data-toggle'));
        $trigger.on('click.up', function () {
            var $t = $(this);
            $t.removeClass('this');
            $target.slideUp('fast');
        });
        $trigger.on('click', function () {
            clearSelection();
            $trigger.toggleClass('this').siblings().each(function () {
                var $t = $(this);
                if ($t.hasClass('this'))$t.trigger('click.up');
            });
            $target.slideToggle('fast');
        });
    });
    //инициализация товаров при загрузке страницы (dom.ready)
    $('.catalog-item.not-inited').each(function () {//все неинициализированные товары
        new itemWrapper(this);
    });
    //карусель
    $('.catalog-carousel').each(function () {
        var ca = $(this),//блок карусельки
            childs = ca.find('> div'),//все дети - div слайды
            h = 0,//высота карусельки
            w = 0,//ширина всех слайдов
            current = 0,//текущая позиция
            total = childs.length,//всего слайдов
            visible = 0,//видимая позиция
            wMin = ca.width(),//минимальная ширина контейнера-скроллера = родитескому блоку
            wrap = $('<div>', {class: 'wrap'}),//перемещаемый блок
            trim = $('<div>', {class: 'trim'}),//обрезатор. должен быть больше окна, т.к. есть badges
            step = 1,//шаг прокрутки. лучше не трогать, т.к. тут не навешаны исключения на граничных слайдах
            time,//таймер авто-прокрутки
            interval = 2000,//интервал авто-прокрутки
            cursorIn = false;//находится ли курсор внутри карусели
        ca.wrapInner(trim);//оборачиваем элементы блоком обрезания
        trim = $(ca.find('.trim'));//пере-инициализируем переменную после оборота )) jQuery такой веселый!
        trim.wrapInner(wrap);//оборачиваем элемены блоком - лентой - длинным, однострочным
        wrap = $(trim.find('.wrap'));//пере-инициализируем переменную
        setTimeout(function () {//dom успокоился
            childs.each(function () {//парсим слайды
                var child = $(this);
                h = Math.max(h, child.height());//считаем высоту карусели как наибольшую из всех
                w += child.outerWidth(true);//считаем длину карусели как ширина слайда с отступами
                if (w < wMin) {//если слайд влез во вьюпорт
                    current++;//мы его видим
                    visible++;//длина вьюпорта
                }
            });
            if (w > wMin) {//если лента длинее карусели
                var next = function () {//следующий слайд
                    var $next = current + step;
                    if ($next >= total) {
                        $next = visible;
                    }
                    var pos = $next - visible;
                    wrap.css({//двигаем ленту. анимацией озабочен css
                        left: ('-' + $(childs[pos]).position().left + 'px')//крайнее положение необходимого слайда
                    });
                    current = $next;
                };
                var prev = function () {//предыдущий слайд
                    var $prev = current - step;
                    if ($prev < visible) {
                        $prev = total - 1;
                    }
                    var pos = $prev - visible;
                    wrap.css({
                        left: ('-' + $(childs[pos]).position().left + 'px')
                    });
                    current = $prev;
                };
                var cPrev = $('<div>', {class: 'control prev', text: '<'});//кнопка "назад"
                cPrev.on('click', function (e) {
                    e.preventDefault();
                    clearInterval(time);//снос авто-переключения
                    clearSelection();
                    prev();
                });
                cPrev.appendTo(ca);
                var cNext = $('<div>', {class: 'control next', text: '>'});//кнопка "вперед"
                cNext.on('click', function (e) {
                    e.preventDefault();
                    clearInterval(time);//снос авто-переключения
                    clearSelection();
                    next();
                });
                cNext.appendTo(ca);
                time = setInterval(function () {//авто-переключение слайдов
                    if (!cursorIn)next();
                }, interval);
            }
            ca.css('height', h + 'px');//установка высоты карусели
            wrap.css({//установка длины ленты
                width: Math.max(w, wMin) + 'px'
            });
            ca.on('mouseenter',function () {
                return cursorIn = true;
            }).on('mouseleave', function () {
                return cursorIn = false;
            });
            ca.addClass('loaded');//завершаем загрузку
        }, 0);
    });
    $('.filter').each(function () {//няшки с фильтрами
        var filter = $(this);
        filter.find('div').each(function () {
            var block = $(this),
                title = block.find(' > label'),
                inner = block.find('.inner'),
                id = block.attr('class').match(/id_[^\s]*/im);
            if (id === null || id.length == 0) return;
            id = id[0];
            var state = localStorage ? localStorage.getItem(id) : false,
                applyState = function () {
                    if (state == 'min') {
                        return block.addClass(state);
                        inner.slideUp(300, function () {
                            return block.addClass(state);
                        });
                    }
                    else {
                        return block.removeClass('min');
                        inner.slideDown(300, function () {
                            return block.removeClass('min');
                        });
                    }
                },
                changeState = function () {
                    if (state == 'min') {
                        state = false;
                    }
                    else {
                        state = 'min';
                    }
                    localStorage ? localStorage.setItem(id, state) : null;
                    applyState();
                };
            title.append($('<span>', {
                class: 'state'
            }));
            applyState();
            title.on('click', function (e) {
                e.preventDefault();
                clearSelection();
                changeState();
            });
        });
    });

    document.getElementById('blueimp').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = $(this).find('a:has(img)');
        if (target.tagName.toLowerCase() === 'img' && target.className === '') {
            blueimp.Gallery(links, options);
        }
    };

    // categories list sublist
    var $categories = $('.categories-list'),
        $body   = $('body');
    if ($categories.length) {
        var subAnchor = 'sub-trigger',
            sub = '.sub-category';
        $categories.find(sub).each(function () {
            var subCategory = $(this),
                $parent = subCategory.parent();
            subCategory.css({
                position: 'absolute',
                'z-index': -10,
                visibility: 'hidden',
                display: 'block',
                height: 'auto',
                transition: 'none',
                '-webkit-transition': 'none'
            }).appendTo($body);

            setTimeout(function () {
                subCategory.data('height', subCategory.outerHeight());
                subCategory.appendTo($parent);
                subCategory.removeAttr('style');

                var subTrigger = subCategory.prev('a[href]');
                subTrigger.addClass(subAnchor);
            }, 180); // таймаут дожен быть больше, чем время css-transition(height)
        });

        var subCategoryCurrent;

        $categories.click(function (e) {
            var $target = $(e.target);
            if ($target.hasClass(subAnchor)) {
                e.preventDefault();
                var subCategory = $target.next(sub);
                if (subCategory.hasClass('shown')) {
                    subCategory.removeAttr('style').removeClass('shown');
                    subCategoryCurrent = null;
                } else {
                    if (subCategoryCurrent) {
                        subCategoryCurrent.removeAttr('style').removeClass('shown');
                    }
                    subCategory.css('height', subCategory.data('height') + 'px').addClass('shown');
                    subCategoryCurrent = subCategory;
                }
            }
        });
    }
    //! categories list sublist

    // public beta mode

    var publicBetaKey = 'publicBetaShown',
        publicBetaText = $('<div>', {
            class: 'block block-white block-slim',
            html: '<div class="content item-page">Мы только запустились и некоторые разделы еще не работают:) Просьба обо всех ошибках писать на почту <a href="mailto:revert.boardshop@gmail.com">revert.boardshop@gmail.com</a>!<br />Спасибо за понимание!:)</div>'
        }),
        publicBeta = function () {
            Modal.pop(publicBetaText, function () {
                localStorage && localStorage.setItem(publicBetaKey, true);
            });
        },
        publicBetaIcon = $('<div>', {
            class: 'beta-icon',
            click: publicBeta,
            title: 'Beta',
            text: 'ϐ'
        });

    $('body').append(publicBetaIcon);

    localStorage && !localStorage.getItem(publicBetaKey) && (function () {
        publicBeta();
    })();

    //! public beta mode

});