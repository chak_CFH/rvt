# Revert boardshop project

##[Jade шаблонизатор](http://jade-lang.com/)

> Используем JADE шаблоны вместо HTML - меньше кодить, валидный html на выходе, встроенное сжатие



##[LESS module](https://github.com/andreyfedoseev/django-less)

### ACHTUNG!!!
> Проект будет валится с ошибкой отсутсвия файла style.less. Необходимо поставить компилятор LESS через менеджер пакетов от node.js:
>> npm i -g less

> Рекомендации по настройке кэша LESS
>
> ###settings.py
>
> LESS_ROOT = os.path.join(PATH, 'static')
>
> LESS_OUTPUT_DIR = 'css'

### ACHTUNG 2!!!

> В проект добавлены новые приложения без которых он не запустится. Эти приложения добавлены в файлик - requirements.txt
>
>На всякий случай перечислю их названия:
>
>django-taggit==0.11.2
>
>django-tinymce==1.5.2
>
>pytils - https://github.com/j2a/pytils - ставится так - python setup.py install


### STATIC
> Статика пизданутая: ресурсы для css (шрифты, спрайты) лежат в rvt/static/css, ресурсы js, less лежат в www/static

### ACHTUNG 3!!!

> Установть библиотеку для работы и картинками - pip install Pillow
>
> Установить аппу для миниатурок - pip install sorl-thumbnail
>
> Для файлбраузер нужно установить его :) - https://github.com/wardi/django-filebrowser-no-grappelli
> ставится через - python setup.py install
>
> После установки фалбраузер - нужно выполнить помманду - python manage.py collectstatic
>
> Нужно установить приложение для менюшек - https://github.com/rossp/django-menu
>
> Для обновленной админки нужно установить - pip install django-admin-tools, а после установки - python manage.py syncdb
>
> Добавлен виджет для выбора цвет, установка - pip install django-paintstore

### ACHTUNG 4!!!
На ubuntu, перед установкой пакета Django-mysql, надо поставить `sudo apt-get install libmysqlclient-dev`

### ACHTUNG 5!!!
Для импорта фикстур, надо написать по буквам `./manage.py loaddata rvt/fixtures/all.json`