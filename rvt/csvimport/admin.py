# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.csvimport.models import *
from adminsortable.admin import SortableAdmin
from adminsortable.admin import SortableTabularInline

class ParseItemsTabularInline(SortableTabularInline):
    """Your inline options go here"""
    model = ParserItems

class ParserAdminClass(SortableAdmin):
    """Any admin options you need go here"""
    inlines = (ParseItemsTabularInline,)

admin.site.register(Parser, ParserAdminClass)

admin.site.register(ParserItems)