# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from rvt.csvimport.views import get_parser_fields, getCSV, import_parser_fin

urlpatterns = patterns('',
    url(r'^upload_parser_file/$', getCSV, name='upload_parser_file'),
	url(r'^get_parser_fields/$', get_parser_fields, name='get_parser_fields'),
    url(r'^import_parser_fin/$', import_parser_fin, name='import_parser_fin'),
)