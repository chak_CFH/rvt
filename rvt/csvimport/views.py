# -*- coding: utf-8 -*-
import re
import os
from django.conf import settings
from django.shortcuts import render
from random import randint
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.contrib.admin.views.decorators import staff_member_required
from django.core.urlresolvers import reverse
from rvt.csvimport.forms import ParserForm, CategoryesForm
from rvt.csvimport.models import Parser
from rvt.items.models import (
    Brand, Color, SizeVal, Item, Specification, Category,
    SizeType, ColorPaint,
)

'''
1) составить список полей в админке [title, brand, model, size, count, price, discount]
'''

@staff_member_required
def getCSV(request):

    if request.method == 'POST':
        form = ParserForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(get_parser_fields))
    else:
        form = ParserForm()
    return render(request, 'get_csv.html', {
        'form': form,
    })

@staff_member_required
def get_parser_fields(request):
    file_name = Parser.objects.latest('id')
    last_file = os.path.join(settings.CURRENT_PATH, 'www', 'media', file_name.csv_file.name)
    f = open(last_file, 'r', buffering=-1)
    l = [line.strip().split(';') for line in f]
    head_csv = l[0]

    import_list = ['name_sex','brand','model','price','color','size','discount','season']

    cat_list = [
                'Оборудование',
                'Одежда',
                'Обувь',
                'Одежда для сноуборда',
                'Аксессуары',
                'Оптика',
                'Краска',
                'Маркеры',
                'Заправка',
                'Аксессуары (GRAFFITI SHOP)'
                ]

    form = CategoryesForm()
    return render(request, 'get_parser_fields.html', {
        'head_csv': head_csv,
        'import_list': import_list,
        'form': form,
        'cat_list': cat_list,
    })

@staff_member_required
def import_parser_fin(request):

    file_name = Parser.objects.latest('id')
    last_file = os.path.join(settings.CURRENT_PATH, 'www', 'media', file_name.csv_file.name)
    f = open(last_file, 'r', buffering=-1)
    l = [line.strip().split(';') for line in f]

    l2 = l[1:len(l)]

    get_category = int(request.GET.get('category', 0))
    get_brand = int(request.GET.get('brand', 0))
    get_name_sex = int(request.GET.get('name_sex', 0))
    get_color = int(request.GET.get('color', 0))
    get_size = int(request.GET.get('size', 0))
    get_model = int(request.GET.get('model', 0))
    get_price = int(request.GET.get('price', 0))
    get_discount = int(request.GET.get('discount', 0))
    get_season = int(request.GET.get('season', 0))
    get_conv = request.GET.get('conv', 0)

    cat_type_list = [
                    'equipment',
                    'clothes',
                    'shoes',
                    'snowboard_wear',
                    'accessories',
                    'optics',
                    'graffiti_paint',
                    'graffiti_markers',
                    'graffiti_refill',
                    'graffiti_accessories'
                    ]

    for item in l2:
        try:
            brand = Brand.objects.get(name=item[get_brand])
        except ObjectDoesNotExist:
            brand = Brand(name=item[get_brand])
            brand.save()

        sex = None
        sex = ""
        #если краска, то пропускаем определение пола
        if get_category < 6 :
            if item[get_name_sex] != "":
                sex = "U"
                product1  = item[get_name_sex].split()
                it_num = 0
                i = 0
                #ищем признак пола
                for word in product1:
                    if word == "М":
                        sex = "M"
                        i = it_num
                        category = Category.objects.get(slug='mans_'+cat_type_list[get_category])
                        break
                    elif word == "Ж":
                        sex = "W"
                        i = it_num
                        q = 'women_'+cat_type_list[get_category]
                        #assert False
                        category = Category.objects.get(slug='women_'+cat_type_list[get_category])
                        break
                    elif word == "ДЕТ":
                        sex = "K"
                        i = it_num
                        category = Category.objects.get(slug='kids_'+cat_type_list[get_category])
                        break
                    it_num += 1

                product = ""
                # если найден пол, то склеиваем все, короме элемента с полом
                it_num = 0

                if sex != "U":
                    for word in product1:
                        if it_num !=i:
                            product = product+word+" "
                        it_num += 1
                else:
                    #если ничего не изменилось, то оставляем полностью строку
                    product = item[get_name_sex]
                    category = Category.objects.get(slug='mans_'+cat_type_list[get_category])
                    category2 = Category.objects.get(slug='women_'+cat_type_list[get_category])

            else:
                pass
                # завершаем обработку этой строки, переходим на новую итерацию
        else:
            #если краска, то ищем сразу по категории
            product = item[get_name_sex]
            category = Category.objects.get(slug=cat_type_list[get_category])

        price = int(float(item[get_price]) * float(get_conv))
        discount = int(item[get_discount]) if item[get_discount] else None

        # находим цвет, если такого цвета нет, то создаем
        if get_category > 5:
            try:
                color = ColorPaint.objects.get(title=item[get_color])
            except ObjectDoesNotExist:
                color = ColorPaint(title=item[get_color])
                color.save()
        else:
            try:
                color = Color.objects.get(title=item[get_color])
            except ObjectDoesNotExist:
                color = Color(title=item[get_color])
                color.save()

        #размер сохраняется в любом случае
        try:
            size = SizeVal.objects.get(value=item[get_size])
        except ObjectDoesNotExist:
            size = SizeVal(
                value=item[get_size],
                size_type=SizeType.objects.get(id=1),
            )
            size.save()

        #ищем товар с текущей моделью, если не находим, то создаем новый товар
        try:
            item_prod = Item.objects.get(item_model=item[get_model])
            item_prod.discount = discount
            item_prod.price = price
            if sex == "U":
                item_prod.woman = True
            item_prod.save()
            if not item_prod.specification_set.all():
                if get_category > 5:
                    specification = Specification(
                        item=item_prod,
                        paint_color=color,
                    )
                else:
                    specification = Specification(
                        item=item_prod,
                        color=color,
                    )
                specification.save()
                specification.item = item_prod
                if get_category > 5:
                    specification.paint_color = color
                else:
                    specification.color = color
                specification.size_list.add(size)
                specification.save()
            else:

                try:
                    if get_category > 5:
                        specification = item_prod.specification_set.get(paint_color__title=color)
                        specification.paint_color = color
                    else:
                        specification = item_prod.specification_set.get(color__title=color)
                        specification.color = color

                    specification.save()
                    specification.size_list.add(size)
                    specification.save()
                except ObjectDoesNotExist:
                    if get_category > 5:
                        specification = Specification(
                            item=item_prod,
                            paint_color=color,
                        )
                    else:
                        specification = Specification(
                            item=item_prod,
                            color=color,
                        )
                    specification.save()
                    specification.item = item_prod
                    if get_category > 5:
                        specification.paint_color = color
                    else:
                        specification.color = color
                    specification.size_list.add(size)
                    specification.save()
            
                # item_prod = Item.objects.get(item_model=item[get_model]+" W")
                # if not item_prod.specification_set.all():
                #     if get_category > 5:
                #         specification = Specification(
                #             item=item_prod,
                #             paint_color=color,
                #         )
                #     else:
                #         specification = Specification(
                #             item=item_prod,
                #             color=color,
                #         )
                #     specification.save()
                #     specification.item = item_prod
                #     if get_category > 5:
                #         specification.paint_color = color
                #     else:
                #         specification.color = color
                #     specification.size_list.add(size)
                #     specification.save()
                # else:

                #     try:
                #         if get_category > 5:
                #             specification = item_prod.specification_set.get(paint_color__title=color)
                #             specification.paint_color = color
                #         else:
                #             specification = item_prod.specification_set.get(color__title=color)
                #             specification.color = color
                #         specification.save()
                #         specification.size_list.add(size)
                #         specification.save()
                #     except ObjectDoesNotExist:
                #         if get_category > 5:
                #             specification = Specification(
                #                 item=item_prod,
                #                 paint_color=color,
                #             )
                #         else:
                #             specification = Specification(
                #                 item=item_prod,
                #                 color=color,
                #             )
                #         specification.save()
                #         specification.item = item_prod
                #         if get_category > 5:
                #             specification.paint_color = color
                #         else:
                #             specification.color = color
                #         specification.size_list.add(size)
                #         specification.save()
        except ObjectDoesNotExist:

            item_prod = Item(
                category=category,
                name=product,
                sex=sex,
                brand=brand,
                item_model=item[get_model],
                price=price,
                sold_count=randint(50, 150),
                discount=discount,
            )
            if sex == "U":
                item_prod.woman = True
            item_prod.save()

            if get_category > 5:
                specification = Specification(
                    item=item_prod,
                    paint_color=color,
                )
            else:
                specification = Specification(
                    item=item_prod,
                    color=color,
                )
            specification.save()
            specification.item = item_prod
            if get_category > 5:
                specification.paint_color = color
            else:
                specification.color = color
            specification.size_list.add(size)
            specification.save()

            # if sex == "U":
            #     item_prod = Item(
            #         category=category2,
            #         name=product,
            #         sex=sex,
            #         brand=brand,
            #         item_model=item[get_model]+" W",
            #         price=price,
            #         sold_count=randint(50, 150),
            #         discount=discount,
            #     )
            #     item_prod.save()
            #     if get_category > 5:
            #         specification = Specification(
            #             item=item_prod,
            #             paint_color=color,
            #         )
            #     else:
            #         specification = Specification(
            #             item=item_prod,
            #             color=color,
            #         )
            #     specification.save()
            #     specification.item = item_prod
            #     if get_category > 5:
            #         specification.paint_color = color
            #     else:
            #         specification.color = color
            #     specification.size_list.add(size)
            #     specification.save()

    return render(request, 'import_parser_fin.html', )