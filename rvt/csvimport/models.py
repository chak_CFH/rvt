# -*- coding: utf-8 -*-
from django.db import models
import re
import os
from django.conf import settings
from random import randint
from adminsortable.models import Sortable
from adminsortable.fields import SortableForeignKey
from django.core.exceptions import ObjectDoesNotExist
from rvt.items.models import Item, Specification
from rvt.items.models import Category
from rvt.items.models import Brand, Color, SizeVal, SizeType

class Parser(Sortable):
    class Meta(Sortable.Meta):
        pass
    title = models.CharField(max_length=255)
    csv_file = models.FileField(upload_to='csv_file/')

    def __unicode__(self):
        return u'%s' % self.title

    # def save(self, *args, **kwargs):
    #     upload_path = os.path.join(settings.CURRENT_PATH, 'www', 'media', 'csv_file', self.csv_file.name)
    #     super(Parser, self).save(*args, **kwargs)
    #
    #     #читаем файл
    #     f = open(upload_path, 'r', buffering=-1)
    #
    #     #формируем список товаров/строк
    #     l = [line.strip().split(';') for line in f]
    #     l2 = l[1:len(l)]
    #
    #     for item in l2:
    #         try:
    #             brand = Brand.objects.get(name=item[3])
    #         except ObjectDoesNotExist:
    #             brand = Brand(name=item[3])
    #             brand.save()
    #
    #         product = item[2].split()
    #         price = int(float(item[-1]) * 35)
    #
    #         try:
    #             color = Color.objects.get(title=item[5])
    #         except ObjectDoesNotExist:
    #             color = Color(title=item[5])
    #             color.save()
    #
    #         try:
    #             size = SizeVal.objects.get(value=item[6])
    #         except ObjectDoesNotExist:
    #             size = SizeVal(
    #                 value=item[6],
    #                 size_type=SizeType.objects.get(id=1),
    #             )
    #             size.save()
    #
    #         try:
    #             item_prod = Item.objects.get(item_model=item[4])
    #             if not item_prod.specification_set.all():
    #                 specification = Specification(
    #                     item=item_prod,
    #                     color=color,
    #                 )
    #                 specification.save()
    #                 specification.item = item_prod
    #                 specification.color = color
    #                 specification.size_list.add(size)
    #                 specification.save()
    #             else:
    #
    #                 try:
    #                     specification = item_prod.specification_set.get(color__title=color)
    #                     specification.color = color
    #                     specification.save()
    #                     specification.size_list.add(size)
    #                     specification.save()
    #                 except ObjectDoesNotExist:
    #                     specification = Specification(
    #                         item=item_prod,
    #                         color=color,
    #                     )
    #                     specification.save()
    #                     specification.item = item_prod
    #                     specification.color = color
    #                     specification.size_list.add(size)
    #                     specification.save()
    #
    #         except ObjectDoesNotExist:
    #
    #             item_prod = Item(
    #                 category=Category.objects.get(slug='mans_clothes'),
    #                 name=product[0],
    #                 sex=product[1],
    #                 brand=brand,
    #                 item_model=item[4],
    #                 price=price,
    #                 sold_count=randint(50, 150),
    #             )
    #             item_prod.save()
    #
    #             specification = Specification(
    #                 item=item_prod,
    #                 color=color,
    #             )
    #             specification.save()
    #             specification.item = item_prod
    #             specification.color = color
    #             specification.size_list.add(size)
    #             specification.save()


class ParserItems(Sortable):
    class Meta(Sortable.Meta):
        pass
    title = models.CharField(max_length=255)
    parser = SortableForeignKey(Parser)

    def __unicode__(self):
        return self.title