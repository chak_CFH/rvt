# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Parser'
        db.create_table(u'csvimport_parser', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')(default=1, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'csvimport', ['Parser'])

        # Adding model 'ParserItems'
        db.create_table(u'csvimport_parseritems', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.PositiveIntegerField')(default=1, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('parser', self.gf('adminsortable.fields.SortableForeignKey')(to=orm['csvimport.Parser'])),
        ))
        db.send_create_signal(u'csvimport', ['ParserItems'])


    def backwards(self, orm):
        # Deleting model 'Parser'
        db.delete_table(u'csvimport_parser')

        # Deleting model 'ParserItems'
        db.delete_table(u'csvimport_parseritems')


    models = {
        u'csvimport.parser': {
            'Meta': {'ordering': "['order']", 'object_name': 'Parser'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1', 'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'csvimport.parseritems': {
            'Meta': {'ordering': "['order']", 'object_name': 'ParserItems'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1', 'db_index': 'True'}),
            'parser': ('adminsortable.fields.SortableForeignKey', [], {'to': u"orm['csvimport.Parser']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['csvimport']