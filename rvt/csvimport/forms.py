# -*- coding: utf-8 -*-
from django import forms
from rvt.csvimport.models import Parser
from rvt.items.models import Category

class ParserForm(forms.ModelForm):
    class Meta:
        model = Parser

class CategoryesForm(forms.Form):
    category = forms.ModelChoiceField(
        queryset=Category.objects.all(),
        required=False,
    )
