# -*- coding: utf-8 -*-
import re
import os
from django.conf import settings
from django.shortcuts import render
from random import randint
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from rvt.csvimport.forms import ParserForm, CategoryesForm
from rvt.csvimport.models import Parser
from rvt.items.models import (
    Brand, Color, SizeVal, Item, Specification, Category,
    SizeType,
)

'''
1) составить список полей в админке [title, brand, model, size, count, price, discount]
'''

def getCSV(request):

    if request.method == 'POST':
        form = ParserForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(get_parser_fields))
    else:
        form = ParserForm()
    return render(request, 'get_csv.html', {
        'form': form,
    })

def get_parser_fields(request):
    file_name = Parser.objects.latest('id')
    last_file = os.path.join(settings.CURRENT_PATH, 'www', 'media', file_name.csv_file.name)
    f = open(last_file, 'r', buffering=-1)
    l = [line.strip().split(';') for line in f]
    head_csv = l[0]

    import_list = ['name_sex','brand','model','price','color','size','discount','season']

    cat_list = [
                'Оборудование',
                'Одежда',
                'Обувь',
                'Одежда для сноуборда',
                'Аксессуары',
                'Оптика',
                'Краска',
                'Маркеры',
                'Заправка',
                'Аксессуары (GRAFFITI SHOP)'
                ]

    form = CategoryesForm()
    return render(request, 'get_parser_fields.html', {
        'head_csv': head_csv,
        'import_list': import_list,
        'form': form,
        'cat_list': cat_list,
    })

def import_parser_fin(request):

    file_name = Parser.objects.latest('id')
    last_file = os.path.join(settings.CURRENT_PATH, 'www', 'media', file_name.csv_file.name)
    f = open(last_file, 'r', buffering=-1)
    l = [line.strip().split(';') for line in f]

    l2 = l[1:len(l)]

    get_category = int(request.GET.get('category', 0))
    get_brand = int(request.GET.get('brand', 0))
    get_name_sex = int(request.GET.get('name_sex', 0))
    get_color = int(request.GET.get('color', 0))
    get_size = int(request.GET.get('size', 0))
    get_model = int(request.GET.get('model', 0))
    get_price = int(request.GET.get('price', 0))
    get_discount = int(request.GET.get('discount', 0))
    get_season = int(request.GET.get('season', 0))

    cat_type_list = [
                    'equipment',
                    'clothes',
                    'shoes',
                    'snowboard_wear',
                    'accessories',
                    'optics',
                    'graffiti_paint',
                    'graffiti_markers',
                    'graffiti_refill',
                    'graffiti_accessories'
                    ]

    for item in l2:
        try:
            brand = Brand.objects.get(name=item[get_brand])
        except ObjectDoesNotExist:
            brand = Brand(name=item[get_brand])
            brand.save()

        # re_obj = re.compile(u'( М| Ж| ДЕТ)')
        # product = re.sub(re_obj,'',item[get_name_sex])
        # unicode_get_name_sex = item[get_name_sex].decode('utf-8')
        # sex = re_obj.search(unicode_get_name_sex).group()

        product1 = item[get_name_sex].split()

        # product1[0] - название модели
        # product1[1] - Пол

        product = product1[0] 
        try:
            sex = unicode(product1[1], 'utf8') #product1[1] #\.decode('utf-8') #unicode(product1[1], 'utf8')
        except IndexError:
            sex = ''
        # aaa = type(sex)
        # aaa1 = type('М')
        varM = sex == u'М'
        varW = sex == u'Ж'
        varK = sex == u'ДЕТ'
        # print('mans_'+cat_type_list[get_category])
        # category = Category.objects.get(slug='mans_'+cat_type_list[get_category])
        # assert False
        if varM:
            # mans
            category = Category.objects.get(slug='mans_'+cat_type_list[get_category])
            sex = 'M'
        elif varW:
            # women
            category = Category.objects.get(slug='women_'+cat_type_list[get_category])
            sex = 'W'
        elif varK:
            # kids
            category = Category.objects.get(slug='kids_'+cat_type_list[get_category])
            sex = 'K'
        else:
            sex = 'U'
            category = Category.objects.get(slug=cat_type_list[get_category])

        #category = Category.objects.get(slug='mans_clothes')

        price = int(float(item[get_price]) * 35)

        try:
            color = Color.objects.get(title=item[get_color])
        except ObjectDoesNotExist:
            color = Color(title=item[get_color])
            color.save()

        try:
            size = SizeVal.objects.get(value=item[get_size])
        except ObjectDoesNotExist:
            size = SizeVal(
                value=item[get_size],
                size_type=SizeType.objects.get(id=1),
            )
            size.save()

        try:
            item_prod = Item.objects.get(item_model=item[get_model])
            if not item_prod.specification_set.all():
                specification = Specification(
                    item=item_prod,
                    color=color,
                )
                specification.save()
                specification.item = item_prod
                specification.color = color
                specification.size_list.add(size)
                specification.save()
            else:

                try:
                    specification = item_prod.specification_set.get(color__title=color)
                    specification.color = color
                    specification.save()
                    specification.size_list.add(size)
                    specification.save()
                except ObjectDoesNotExist:
                    specification = Specification(
                        item=item_prod,
                        color=color,
                    )
                    specification.save()
                    specification.item = item_prod
                    specification.color = color
                    specification.size_list.add(size)
                    specification.save()

        except ObjectDoesNotExist:

            item_prod = Item(
                category=category,
                name=product,
                sex=sex,
                brand=brand,
                item_model=item[get_model],
                price=price,
                sold_count=randint(50, 150),
            )
            item_prod.save()

            specification = Specification(
                item=item_prod,
                color=color,
            )
            specification.save()
            specification.item = item_prod
            specification.color = color
            specification.size_list.add(size)
            specification.save()

    return render(request, 'import_parser_fin.html', )