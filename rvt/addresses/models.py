# -*- coding: utf-8 -*-
from django.db import models
from rvt.accounts.models import User

class Address(models.Model):
    flat_number = models.IntegerField(u'Номер квартиры')
    street = models.CharField(u'Улица', max_length=60)
    house_number = models.IntegerField(u'Номер дома')
    house_porch = models.IntegerField(u'Номер подъезда')
    city = models.CharField(u'Город', default=u'Тюмень', max_length=60)
    address_default = models.ForeignKey(User, verbose_name=u'Пользователь')
    permanent_address = models.BooleanField(u'Постоянный адрес', default=False)

    class Meta:
        verbose_name = u'Адрес пользователя'
        verbose_name_plural = u'Адреса пользователей'

    def __unicode__(self):
        return u'%s, %s %s, кв. %s' % (
            self.city,
            self.street,
            self.house_number,
            self.flat_number,
        )