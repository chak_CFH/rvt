# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.orders.models import Orders, OrderItem, OrderStatus

class OrderItemInline(admin.StackedInline): #StackedInline):
    # form = PhotoAdminForm
    model = OrderItem
    max_num=0 #10
    extra=0

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('order', 'item', 'spec', 'address', 'count', 'color', 'size')
        return self.readonly_fields

class OrdersAdmin(admin.ModelAdmin):
    list_filter = ['status__status_name',]
    list_display = ['order_number', 'date', 'status', 'get_username', 'get_userphone', 'get_address_shipping', 'order_sum']
    inlines = [OrderItemInline, ]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('user', 'order_number', 'date', 'sum')
        return self.readonly_fields

class OrderItemAdmin(admin.ModelAdmin):
    list_display = ('item_number', 'orders_date', 'image_preview', 'address', 'status', 'user', 'user_number', 'item', 'item_model', 'count', 'color', 'size')
    list_display_links = ('item_number', )
    search_fields = ('order__order_number', 'order__user__email', 'item__item_model', 'item__name', 'item__brand__name')
    # list_filter = ('order', 'item', )

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('order', 'item', 'spec', 'address', 'count', 'color', 'size')
        return self.readonly_fields

admin.site.register(Orders, OrdersAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
admin.site.register(OrderStatus)
