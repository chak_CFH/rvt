# -*- coding: utf-8 -*-
import string
import random
from django.db import models
from rvt.accounts.models import User
from rvt.items.models import Item, Specification

def order_number_generator(size=8, chars=string.digits):
    return ''.join(random.choice(chars) for x in range(size))

class OrderStatus(models.Model):
    status_name = models.CharField(u'Статус', max_length=60)

    class Meta:
        verbose_name = u'Статус заказа'
        verbose_name_plural = u'Статус заказа'

    def __unicode__(self):
        return self.status_name

class Orders(models.Model):
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    order_number = models.CharField(
        u'Номер заказа',
        max_length=8,
        blank=True,
        help_text=u'Автоматическая генерация номера заказа',
    )
    date = models.DateTimeField(u'Дата заказа', auto_now=True)
    status = models.ForeignKey(OrderStatus, verbose_name=u'Статус', default=1, blank=True, null=True)
    sum = models.IntegerField(u'Сумма', default=0)

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказ'

    def __unicode__(self):
        return '%s %s' % (self.user, self.order_number)

    def order_sum(self):
        user_order = self.orderitem_set.filter(order__order_number=self.order_number)
        sum_list = []
        for item in user_order:
            sum_list.append(item.item.price * item.count)
        return sum(sum_list)
    order_sum.short_description = u'Сумма заказа'

    def count(self):
        return self.objects.count()

    def get_username(self):
        return self.user.username()
    get_username.short_description = u'ФИО'

    def get_userphone(self):
        return self.user.mobile_phone
    get_userphone.short_description = u'Телефон'

    def get_address_shipping(self):
        address = None
        for item in self.user.address_set.filter(permanent_address=True):
            address = item
        return address
    get_address_shipping.short_description = u'Адрес доставки'

    def save(self, *args, **kwargs):
        if not self.order_number:
            self.order_number = order_number_generator()
        self.sum = self.order_sum()
        super(Orders, self).save(*args, **kwargs)

class OrderItem(models.Model):
    order = models.ForeignKey(Orders, verbose_name=u'Заказ')
    item = models.ForeignKey(Item, verbose_name=u'Товар')
    spec = models.ForeignKey(Specification, verbose_name=u'Спецификация', blank=True, null=True)
    address = models.CharField(u'Адрес отправки' ,max_length=255, blank=True, null=True)
    count = models.IntegerField(u'Кол-во товара')
    color = models.TextField(u'Цвет товара', max_length=30)
    size = models.CharField(u'Размер товара', max_length=30)

    class Meta:
        verbose_name = u'Заказ(Товар)'
        verbose_name_plural = u'Заказ(Товар)'

    def __unicode__(self):
        return '%s %s' % (self.order.order_number, self.item.name)

    def order_item_sum(self):
        return self.item.price * self.count

    def item_model(self):
        return self.item.item_model
    item_model.short_description = u'Модель'

    def user(self):
        return self.order.user.email
    user.short_description = u'Пользователь'

    def item_number(self):
        return self.order.order_number
    item_number.short_description = u'№ Заказа'

    def status(self):
        return self.order.status
    status.short_description = u'Статус'

    def user_number(self):
        return self.order.user.mobile_phone
    user_number.short_description = u'Телефон'

    def orders_date(self):
        return self.order.date
    orders_date.short_description = u'Дата заказа'

    def image_preview(self):
        if self.spec.specificationimages_set.all():
            image_prev = ''
            for image in self.spec.specificationimages_set.all()[:1]:
                image_prev = '<img src="%s" width="100">' % image.image.url
            return image_prev
        else:
            return '(none)'
    image_preview.short_description = u'Изображение'
    image_preview.allow_tags = True