# -*- coding: utf-8 -*-
from django import forms
from rvt.accounts.models import User
from rvt.addresses.models import Address
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from sorl.thumbnail import ImageField
from rvt.img_sorl import AdminImageWidget
from django.contrib.auth.forms import AuthenticationForm

# class AuthForm(forms.Form):
#     login = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': u'Логин'}))
#     password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': u'Пароль'}))

class AuthForm(AuthenticationForm):
    pass

    def __init__(self, *args, **kwargs):
        super(AuthForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'some-class other-class'

    def as_div(self):
        return self._html_output(
            normal_row = u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row = u'<div class="error">%s</div>',
            row_ender = '</div>',
            help_text_html = u'<div class="hefp-text">%s</div>',
            errors_on_separate_row = False)

class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'Пароль еще раз', widget=forms.PasswordInput)
    activate_code = forms.CharField(label=u'Код активации', widget=forms.HiddenInput, required=False)
    error_css_class = 'class-error'
    required_css_class = 'class-required'

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'some-class other-class'

    def as_div(self):
        return self._html_output(
            normal_row = u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row = u'<div class="error">%s</div>',
            row_ender = '</div>',
            help_text_html = u'<div class="hefp-text">%s</div>',
            errors_on_separate_row = False)

    class Meta:
        model = User
        fields = ('email', 'name', 'surname', 'mobile_phone', )

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """ Форма для редактирования пользователя. """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ['email', 'password', ]

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class UserChangeFormAdmin(forms.ModelForm):
    """ Форма для редактирования пользователя в админке. """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ['email',
                  'password',
                  'is_active',
                  'is_superuser',
                  'is_staff',
                  'activate_code',
                  'last_login',
                  'date_joined',
                  'user_permissions',
                  'name',
                  'surname',
                  'mobile_phone',
                  'image',
                  'regular_customer',
        ]
        widgets = {'image': AdminImageWidget}

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class EmailForForgottenPassword(forms.Form):
    email = forms.EmailField(label=u'email')

class ForgottenPasswordForm(forms.Form):
    """ Класс формы для восстановления пароля """
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

class AddAddressForm(forms.ModelForm):
    ''' Форма для добавления адресов пользователя. '''
    def __init__(self, *args, **kwargs):
        super(AddAddressForm, self).__init__(*args, **kwargs)
        self.fields['address_default'].widget = forms.HiddenInput()
        self.fields['permanent_address'].widget = forms.HiddenInput()
        # adding css classes to widgets without define the fields:
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'value'

    def as_div(self):
        return self._html_output(
            normal_row = u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row = u'<div class="error">%s</div>',
            row_ender = '</div>',
            help_text_html = u'<div class="help-text">%s</div>',
            errors_on_separate_row = False)

    class Meta:
        model = Address

class UserChangeForm(forms.ModelForm):
    ''' Форма для редактирования персонаьных данных. '''
    # image = avatar_forms.AvatarField()

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'some-class other-class'

    def as_div(self):
        return self._html_output(
            normal_row = u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row = u'<div class="error">%s</div>',
            row_ender = '</div>',
            help_text_html = u'<div class="hefp-text">%s</div>',
            errors_on_separate_row = False)

    class Meta:
        model = User
        exclude = ('password', 'last_login', 'date_joined',
            'is_superuser', 'is_admin', 'is_staff', 'is_active', 'user_permissions', 'activate_code', 'groups')
        widgets = {'image': AdminImageWidget}

# class SexyModelForm(forms.ModelForm):
#     error_css_class = 'class-error'
#     required_css_class = 'class-required'
#     def __init__(self, *args, **kwargs):
#         super(ModelForm, self).__init__(*args, **kwargs)
#         # adding css classes to widgets without define the fields:
#         for field in self.fields:
#             self.fields[field].widget.attrs['class'] = 'some-class other-class'
#     def as_div(self):
#         return self._html_output(
#             normal_row = u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
#             error_row = u'<div class="error">%s</div>',
#             row_ender = '</div>',
#             help_text_html = u'<div class="hefp-text">%s</div>',
#             errors_on_separate_row = False)