# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, Permission, PermissionsMixin, Group
)
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import _user_has_perm, _user_has_module_perms

class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email,
            password=password,
        )
        user.is_superuser = True
        user.is_active = True
        user.is_staff = True
        user.name = u'Admin'
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(
        verbose_name=u'email адрес',
        max_length=255,
        unique=True,
    )
    is_active = models.BooleanField(u'Активный пользователь', default=False)
    is_superuser = models.BooleanField(u'Администратор', default=False)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_admin = models.BooleanField(default=True)
    image = models.ImageField(u'Аватар', upload_to='user_avatar/', blank=True, editable=True)
    name = models.CharField(u'Имя', max_length=40)
    surname = models.CharField(u'Фамилия', max_length=60, blank=True)
    mobile_phone = models.CharField(u'Мобильный телефон', max_length=11)
    activate_code = models.CharField(u'Код активации', max_length=30, blank=True, null=True)
    date_joined = models.DateTimeField(u'Дата регистрации', default=timezone.now)
    user_permissions = models.ManyToManyField(Permission,
        verbose_name=_('user permissions'), blank=True,
        help_text='Specific permissions for this user.')
    groups = models.ManyToManyField(Group, verbose_name=_('groups'),
        blank=True, help_text=_('The groups this user belongs to. A user will '
                                'get all permissions granted to each of '
                                'his/her group.'))
    regular_customer = models.BooleanField(u'Стать постоянным покупателем', default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['date_of_birth']

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    def username(self):
        return u'%s %s' % (self.name, self.surname)

    def get_full_name(self):
        # The user is identified by their email address
        return u'%s %s' % (self.surname, self.name) # self.email
    get_full_name.short_description = u'ФИО'

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def get_avatar(self):
        if not self.image:
            return False
        return True

    def get_addres(self):
        try:
            addr = self.address_set.get(permanent_address=True)
        except:
            addr = ''
        return addr
    get_addres.short_description = u'адрес'

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return self.email

    # def has_perm(self, perm, obj=None):
    #     "Does the user have a specific permission?"
    #     # Simplest possible answer: Yes, always
    #     return True
    #
    # def has_module_perms(self, app_label):
    #     "Does the user have permissions to view the app `app_label`?"
    #     # Simplest possible answer: Yes, always
    #     return True

    # @property
    # def is_staff(self):
    #     "Is the user a member of staff?"
    #     # Simplest possible answer: All admins are staff
    #     return self.is_admin

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """

        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        # Otherwise we need to check the backends.
        return _user_has_perm(self, perm, obj)
    #
    # def has_perms(self, perm_list, obj=None):
    #     """
    #     Returns True if the user has each of the specified permissions. If
    #     object is passed, it checks if the user has all required perms for this
    #     object.
    #     """
    #     for perm in perm_list:
    #         if not self.has_perm(perm, obj):
    #             return False
    #     return True

    def has_module_perms(self, app_label):
        """
        Returns True if the user has any permissions in the given app label.
        Uses pretty much the same logic as has_perm, above.
        """
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        return _user_has_module_perms(self, app_label)