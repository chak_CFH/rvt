# -*- coding: utf-8 -*-
import string
import random
import datetime

from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.shortcuts import render, get_object_or_404
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponseRedirect, Http404
from django.core.mail import send_mail
from django.forms.models import modelformset_factory

from rvt.accounts.models import User
from rvt.addresses.models import Address
from rvt.orders.models import Orders
from rvt.accounts.forms import (UserCreationForm, ForgottenPasswordForm, EmailForForgottenPassword,
                                AddAddressForm, UserChangeForm, AddAddressForm)

def unique_str_generator(size=6, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

def user_creation(request):
    ''' Регистрация нового пользователя. '''
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            cd = form.cleaned_data
            activate_code = unique_str_generator(30)
            user = User(
                email=cd['email'],
                mobile_phone=cd['mobile_phone'],
                name=cd['name'],
                surname=cd['surname'],
                activate_code=activate_code,
            )
            user.set_password(cd['password1'])
            user.save()

            send_mail(
                u'Регистрация на сайте Revert',
                get_template('registration_message.txt').render(
                    Context({
                        'name': cd['name'],
                        'activate': activate_code,
                        'site': settings.SITE_URL,
                    })
                ),
                'hooper_spd@mail.ru',
                [cd['email'], ]
            )

            return HttpResponseRedirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'user_creation.jade', {
        'form': form,
    })

def mail_attention_confirm(request):
    return render(request, 'mail_attention_confirm.html')

def user_activate(request, activate_code):
    ''' Ссылка для активации нового пользователя. '''
    user = get_object_or_404(User, activate_code = activate_code)
    today = datetime.datetime.today()
    activation_period = today + datetime.timedelta(days=1)

    if user and today.date() <= activation_period.date():
        user.is_active = True
        user.save()

        return HttpResponseRedirect('/')
    else:
        raise Http404

def recovery_password(request):
    ''' Ссылка на восстановление забытого пароля. '''
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = EmailForForgottenPassword(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            email = cd['email']
            user = get_object_or_404(User, email = email)
            user.activate_code = unique_str_generator(30)
            user.save()
            send_mail(
                    u'Восстановление пароля',
                    get_template('new_password.txt').render(
                        Context({
                            'login': user.name,
                            'recovery': user.activate_code,
                            'site': settings.SITE_URL,
                        })
                    ),
                    'hooper_spd@mail.ru',
                    [email]
                )

            return HttpResponseRedirect('/auth/mail_attention_recall/')
    else:
        form = EmailForForgottenPassword()

    return render(request, 'sendpassword.jade', {
        'form': form,
    })

def set_password(request, recovery_key):
    user = get_object_or_404(User, activate_code = recovery_key)
    today = datetime.datetime.today()
    activation_period = today + datetime.timedelta(days=1)

    if user and today.date() <= activation_period.date():
        if request.method == 'POST':
            form = ForgottenPasswordForm(request.POST)
            if form.is_valid():
                usp = form.cleaned_data
                user.set_password(usp['password1'])
                user.activate_code = unique_str_generator(30)
                user.save()
                return HttpResponseRedirect('/')
        else:
            form = ForgottenPasswordForm()

    return render(request, 'user_set_password.html', {
        'form': form,
    })

@login_required
def user_account(request):
    user = get_object_or_404(User, id=request.user.id)
    permanent_address = user.address_set.filter(permanent_address=True)
    other_addresses = user.address_set.exclude(permanent_address=True)
    # AddressFormSet = modelformset_factory(Address, form=AddAddressForm, extra=1)
    if request.method == 'POST':
        formset = AddAddressForm(request.POST)
        if formset.is_valid():
            formset.save()
            return HttpResponseRedirect(reverse(user_account))
    else:
        formset = AddAddressForm(initial=
            {'address_default': request.user},
        )
    return render(request, 'user_account.jade', {
        'user': user,
        'formset': formset,
        'permanent_address': permanent_address,
        'other_addresses': other_addresses,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/accounts/account/','title':'Страница пользователя'}
        ]
    })

@login_required
def edit_user_account(request):
    user = get_object_or_404(User, id=request.user.id)
    next = request.GET.get('next', None)
    if request.method == 'POST':
        form = UserChangeForm(request.POST, request.FILES, instance=user)
        if form.is_valid():
            form.save()
            if next:
                return HttpResponseRedirect(next)
            return HttpResponseRedirect(reverse(user_account))
    else:
        form = UserChangeForm(instance=user)
    return render(request, 'edit_user_account.html', {
        'form': form,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/accounts/account/','title':'Страница пользователя'},
            {'url':'/accounts/edit_user_account/','title':'Редактирование'}
        ]
    })

@login_required
def edit_user_address(request, address_id):
    address = get_object_or_404(Address, id=address_id)
    if address.address_default == request.user:
        if request.method == 'POST':
            form = AddAddressForm(request.POST, instance=address)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse(user_account))
        else:
            form = AddAddressForm(instance=address)
    else:
        return HttpResponseRedirect(reverse(user_account))
    return render(request, 'edit_user_address.html', {
        'form': form,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/accounts/account/','title':'Страница пользователя'},
            {'url':'/accounts/edit_user_address/'+address_id,'title':'Редактирование адреса'}
        ]
    })

@login_required
def delete_user_address(request, address_id):
    address = get_object_or_404(Address, id=address_id)
    if address.address_default == request.user:
        address.delete()
    return HttpResponseRedirect(reverse(user_account))

@login_required
def order_history(request, order_id):
    order = get_object_or_404(Orders, id=order_id)

    return render(request, 'order_history.jade', {
        'order': order,
        'order_sum': order.order_sum,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/accounts/account/','title':'Страница пользователя'},
            {'url':'/accounts/order_history/%s/' % order.id,'title':'Заказ №%s'.decode('utf-8') % order.order_number}
        ]
    })

@login_required
def permanent_address(request, address_id):
    address = get_object_or_404(Address, id=address_id)
    if address.address_default == request.user:
        try:
            permanent_address = request.user.address_set.get(permanent_address=True)
            permanent_address.permanent_address = False

            permanent_address.save()
            address.permanent_address = True
            address.save()
        except:
            address.permanent_address = True
            address.save()

    return HttpResponseRedirect(reverse(user_account))
