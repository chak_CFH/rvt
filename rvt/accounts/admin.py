from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from rvt.accounts.models import User
from rvt.accounts.forms import UserChangeForm, UserCreationForm, UserChangeFormAdmin
from rvt.addresses.models import Address

class DefaultAddressInline(admin.TabularInline):
    model = Address
    # max_num = 1
    extra = 1

class MyUserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeFormAdmin # UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('is_superuser', 'is_staff', 'get_full_name', 'email', 'mobile_phone', 'get_addres', )
    list_display_links = ['get_full_name', 'email', ]
    list_filter = ('is_superuser', 'is_active', )
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('activate_code', 'last_login', 'date_joined', 'name', 'surname', 'image', 'mobile_phone', 'regular_customer', )}),
        ('Permissions', {'fields': ('is_superuser', 'is_active', 'is_staff', 'user_permissions', )}),
    )
    inlines = [DefaultAddressInline, ]
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ('user_permissions', )

# Now register the new UserAdmin...
admin.site.register(User, MyUserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)
