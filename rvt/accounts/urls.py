# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login, logout
from rvt.accounts.views import (user_creation, user_activate, recovery_password,
                                set_password, user_account, delete_user_address,
                                edit_user_account, edit_user_address, order_history, permanent_address)

urlpatterns = patterns('',
   url(r'login/$', login, {'template_name': 'login.jade'}, name='accounts_login'),
   url(r'logout/$', logout, {'next_page': '/'}, name='accounts_logout'),
   url(r'edit_user_account/$', edit_user_account, name='edit_user_account'),
   url(r'edit_user_address/(\d+)/$', edit_user_address, name='edit_user_address'),
   url(r'account/$', user_account, name='user_account'),
   url(r'registration/$', user_creation, name='user_creation'),
   url(r'activation/(\w+)/$', user_activate, name='user_activate'),
   url(r'recovery_password/$', recovery_password, name='new_password'),
   url(r'set_password/(\w+)/$', set_password, name='user_set_password'),
   url(r'delete_address/(\d+)/$', delete_user_address, name='delete_user_address'),
   url(r'order_history/(\d+)/$', order_history, name='order_history'),
   url(r'permanent_address/(\d+)/$', permanent_address, name='permanent_address'),
)