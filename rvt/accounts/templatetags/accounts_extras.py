from django import template
register = template.Library()
import ast

@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class":css})

@register.filter(name='to_dict')
def to_dict(obj):
    spec_dict = ast.literal_eval(obj)
    return spec_dict
