from django import forms
from django.utils.safestring import mark_safe
from sorl.thumbnail import get_thumbnail

class AdminImageWidget(forms.FileInput):

  """ A ImageField Widget for admin that shows a thumbnail. """

  def __init__(self, attrs={}):
    super(AdminImageWidget, self).__init__(attrs)

  def render(self, name, value, attrs=None):

    output = []
    if value and hasattr(value, 'url'):
      thumb = get_thumbnail(value, 'x100', crop='center', format='PNG')
      output.append('<a target="_blank" href="%s"><img src="%s" style="height: 100px;" /></a> '
                     % (value.url, thumb.url))
    output.append(super(AdminImageWidget, self).render(name, value, attrs))
    return mark_safe(u''.join(output))
