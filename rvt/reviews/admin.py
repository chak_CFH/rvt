# -*- coding: utf-8 -*-
from django.contrib import admin

from rvt.reviews.models import Review
from sorl.thumbnail import ImageField
from rvt.img_sorl import AdminImageWidget

class ReviewAdmin(admin.ModelAdmin):
	list_display = ('image_preview', 'title', 'date', )
	search_fields = ['title', ]
	list_filter = ['date', ]
	ordering = ['-date', ]
	formfield_overrides = {
		ImageField: {'widget': AdminImageWidget},
	}

admin.site.register(Review, ReviewAdmin)