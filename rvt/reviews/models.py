
# -*- coding: utf-8 -*-
import re
from django.db import models
from tinymce.models import HTMLField
from sorl.thumbnail import ImageField
from taggit.managers import TaggableManager
from pytils import translit
from django.db import models

from taggit.managers import TaggableManager
from taggit.models import Tag, TaggedItem

class ReviewTag(Tag):
    class Meta:
        proxy = True

    def slugify(self, tag, i=None):
        slug = tag.lower().replace(' ', '-')
        if i is not None:
            slug += '-%d' % i
        return slug

class ReviewTaggedItem(TaggedItem):
    class Meta:
        proxy = True

    @classmethod
    def tag_model(cls):
        return ReviewTag

class Review(models.Model):
    title = models.CharField(u'Заголовок обзора', max_length=30, unique=True)
    slug = models.CharField(u'Заголовок обзора для ЧПУ', max_length=60, unique=True, blank=True, help_text=u'заполнять не нужно, генрится автоматически.')
    date = models.DateField(u'Дата обзора')
    preview = models.TextField(u'Краткий текст')
    content = HTMLField(u'Полный текст')
    image = ImageField(u'Изображение', upload_to='images/reviews/')
    tags = TaggableManager(u'Тэги', through=ReviewTaggedItem)

    class Meta:
        verbose_name = u'Обзор'
        verbose_name_plural = u'Обзоры'

    def image_preview(self):
        if self.image:
            return '<img src="%s" width="100">' % self.image.url
        else:
            return '(none)'
    image_preview.short_description = u'Изображение'
    image_preview.allow_tags = True

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        '''  Переопределенный метод сэйв с сохранением транслита заголовка для ЧПУ. '''
        translit_slug = translit.translify(self.title.replace(' ', '_'))
        self.slug = re.sub(u"[^a-zA-Z0-9\_\s]", "", translit_slug)
        super(Review, self).save(*args, **kwargs)


