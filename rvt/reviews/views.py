# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from rvt.reviews.models import Review

def reviews_list(request):

    tag = request.GET.get("tag", None)
    if tag==None:
        reviews = Review.objects.all()
    else:
        reviews = Review.objects.filter(tags__name__in=[tag])

    return render(request, 'reviews_list.jade', {
        'reviews': reviews,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/reviews','title':'Обзоры'}
        ]
    })

def review_detail(request, review_slug):
    review = Review.objects.get(slug=review_slug)
    return render(request, 'review_detail.jade', {
        'review': review,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/reviews','title':'Обзоры'}
        ]
    })

