# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from rvt.reviews.views import reviews_list, review_detail

urlpatterns = patterns('',
	url(r'^$', reviews_list, name='reviews_list'),
	url(r'^(\w+)/$', review_detail, name='review_detail'),
)