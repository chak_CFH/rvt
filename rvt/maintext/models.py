# -*- coding: utf-8 -*-
from django.db import models
from tinymce.models import HTMLField

class MainText(models.Model):
    text = HTMLField(u'Текст на главной')

    class Meta:
        verbose_name = u'Текст на главной'
        verbose_name_plural = u'Текст на главной'

    def __unicode__(self):
        return u'Текст на главной'
