# -*- coding: utf-8 -*-
from django.db import models
from tinymce.widgets import TinyMCE
from tinymce.models import HTMLField
from sorl.thumbnail import ImageField

class SliderText(models.Model):
    image = ImageField(u'Изображение', upload_to='slider/')
    content = models.CharField(u'Текст', max_length=100, blank=True)
    show = models.BooleanField(u'Показывать на сайте', blank=True)

    class Meta:
        verbose_name = u'Слайдер'
        verbose_name_plural = u'Слайдер'

    def image_preview(self):
        if self.image:
            return '<img src="%s" width="100">' % self.image.url
        else:
            return '(none)'
    image_preview.short_description = u'Изображение'
    image_preview.allow_tags = True

    def __unicode__(self):
        return self.content