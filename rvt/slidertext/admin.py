# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.slidertext.models import SliderText
from sorl.thumbnail import ImageField
from rvt.img_sorl import AdminImageWidget

class SliderTextAdmin(admin.ModelAdmin):
	list_display = ('image_preview', 'content', )
	formfield_overrides = {
		ImageField: {'widget': AdminImageWidget},
	}

admin.site.register(SliderText, SliderTextAdmin)