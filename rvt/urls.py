# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from rvt.contacts.views import contacts

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    (r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/filebrowser/', include('filebrowser.urls')),
    url(r'^$', 'rvt.views.home', name='home'),
    url(r'^contacts/$', contacts, name='contacts'),
    url(r'^', include("rvt.django_ffiler.urls")),
    url(r'^cart', include('rvt.cart.urls')),
    url(r'^csvimport/', include('rvt.csvimport.urls')),

    url(r'^accounts/', include('rvt.accounts.urls')),
    url(r'^', include('rvt.items.urls')),
    url(r'^reviews/', include('rvt.reviews.urls')),
    url(r'^faq/', include('rvt.faq.urls')),
    url(r'^news/', include('rvt.news.urls')),
    url(r'^comments/', include('rvt.comments.urls')),

    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^sitemap.xml/$', 'rvt.views.sitemap', name='sitemap'),

)
if settings.DEBUG404:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    )

# import os
# if settings.DEBUG404:
#     urlpatterns += patterns('',
#         (r'^static/(?P<path>.*)$', 'django.views.static.serve',
#         {'document_root': os.path.join(os.path.dirname(__file__), 'static')} ),
#     )