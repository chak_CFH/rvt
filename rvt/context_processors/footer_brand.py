# -*- coding: utf-8 -*-
from django.conf import settings
from rvt.brands.models import Brand

def footbrands_context(request):
    footbrands = Brand.objects.all()
    return {
        'footbrands': footbrands,
    }