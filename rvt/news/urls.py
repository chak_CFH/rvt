# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from rvt.news.views import news_list, news_detail

urlpatterns = patterns('',
	url(r'^$', news_list, name='news_list'),
	url(r'^(\w+)/$', news_detail, name='news_detail'),
)