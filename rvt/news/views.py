# -*- coding: utf-8 -*-
from django.shortcuts import render
from rvt.news.models import News

def news_list(request):
    tag = request.GET.get("tag", None)
    if tag==None:
        news = News.objects.all()
    else:
        news = News.objects.filter(tags__name__in=[tag])

    return render(request, 'news_list.html', {
        'news': news,
    })

def news_detail(request, review_slug):
    news = News.objects.get(slug=review_slug)
    return render(request, 'news_detail.html', {
        'news': news,
    })