# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from rvt.faq.views import faq_view

urlpatterns = patterns('',
	url(r'^$', faq_view, name='faq_view'),
)