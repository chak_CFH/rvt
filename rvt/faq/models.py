# -*- coding: utf-8 -*-
from django.db import models

class Faq(models.Model):
    question = models.TextField(u'Вопрос')
    username = models.CharField(u'Пользователь', max_length=255)
    email = models.EmailField(u'Email', max_length=255, blank=True)
    answer = models.TextField(u'Ответ', blank=True)
    date = models.DateTimeField(u'Дата', auto_now=True)
    show = models.BooleanField(u'Показывать на сайте', blank=True)

    class Meta:
        verbose_name = u'Вопрос-Ответ'
        verbose_name_plural = u'Вопрос-Ответ'

    def __unicode__(self):
        return self.username
