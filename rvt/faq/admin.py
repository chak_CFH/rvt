# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.faq.models import Faq

admin.site.register(Faq)