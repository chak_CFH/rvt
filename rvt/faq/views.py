# -*- coding: utf-8 -*-
from rvt.faq.models import Faq
from rvt.faq.forms import FaqForm

from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

def faq_view(request):
    faq_list = Faq.objects.filter(show=True)
    if request.method == 'POST':
        form = FaqForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(faq_view))
    else:
        form = FaqForm()

    return render(request, 'faq_list.html', {
        'faq_list': faq_list,
        'form': form,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/faq','title':'Вопрос-ответ'}
        ]
    })