# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from rvt.cart.views import cart_list, user_accuracy, ordering, order, cart_permanent_address, thx

urlpatterns = patterns('',
    url(r'/user_accuracy/$', user_accuracy, name='user_accuracy'),
    url(r'/ordering/$', ordering, name='ordering'),
    url(r'/order/$', order, name='order'),
    url(r'/cart_permanent_address/(\d+)/', cart_permanent_address, name='cart_permanent_address'),
	url(r'/thx/(\d+)/$', thx, name='thx'),
    url(r'/$', cart_list, name='cart'),
)