 # -*- coding: utf-8 -*-
import json
import urllib2
import random
import string
import pynliner
import os

from django.core.mail import send_mail, EmailMultiAlternatives
from django.shortcuts import render, get_object_or_404
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponseRedirect, HttpResponse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import authenticate, login
from django.core.mail import send_mail
from django.db.models import Q

from rvt.accounts.models import User
from rvt.accounts.forms import UserCreationForm, AddAddressForm
from rvt.cart.forms import CartUserCreationForm
from rvt.accounts.views import unique_str_generator
from rvt.addresses.models import Address
from rvt.items.models import Item, Specification, SizeVal
from rvt.cart.models import FreeShipping
from rvt.orders.models import Orders, OrderItem
from rvt.accounts.forms import AuthForm
from rvt.items.models import Color, ColorPaint

def cart_list(request):
    ''' Корзина товаров '''
    step = 1
    uri = request.COOKIES.get('cart', '')

    while True:
        dec = urllib2.unquote(uri)
        if dec == uri:
            break
        uri = dec

    item_range = range(1, 10)
    json_cart = ''
    items_list = ''
    graffiti_list = ''
    graffiti_colors = ''
    fs_cond = ''
    items = ''
    price_sum = ''
    missing_sum = ''
    graffiti_spec = ''
    if uri:
        json_cart = json.loads(uri)

        items_list = [{'spec': Specification.objects.get(id=item['spec']), 'size': SizeVal.objects.get(value=item['size']), 'amount': item['amount']} for item in json_cart['items']]

        graffiti_ids = [item['id'] for item in json_cart['graffiti']]
        price_sum = sum([item['price'] * item['amount'] for item in json_cart['items']] + [item['price'] * item['amount'] for item in json_cart['graffiti']])
        # qwe = json_cart['graffiti'][0]['specs']

        # graffiti_colors = [item['color'] for item in json_cart['graffiti'][0]['specs']]
        # assert False
        graffiti_list = Item.objects.filter(id__in=graffiti_ids)
        graffiti_spec = [({'graffiti': Item.objects.get(id=item['id']), 'color': ColorPaint.objects.filter(title__in=[item2['color'] if item2['amount'] > 0 else None for item2 in item['specs']])}) for item in json_cart['graffiti']]

        fs = FreeShipping.objects.all()

        #балонов нет в списке товаров = тру
        ballon_exist = False

        #проверяем балоны в корзине
        for item in items_list:
            if item['spec'].item.category.slug == 'graffiti_paint':
                ballon_exist = True

        #если есть балон - выводим условие для балона
        if len(fs):
            if ballon_exist:
                fs_cond = fs[0].min_ballon_count
            else:
                fs_cond = fs[0].min_order_summ
        else:
            fs_cond=0

        missing_sum = fs_cond - price_sum

    return render(request, 'cart_list.jade', {
        'cart': json_cart,
        'items_list': items_list,
        'graffiti_list': graffiti_list,
        'graffiti_colors': graffiti_colors,
        'items': items,
        'step': step,
        'fs_cond':fs_cond,
        'price_sum': price_sum,
        'missing_sum': missing_sum,
        'item_range': item_range,
        'graffiti_spec': graffiti_spec,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/cart/','title':'Корзина'}
        ]
    })

def user_accuracy(request):
    '''
        Второй шаг при заказе.
        Логин или регистрация, уточнение данных юзера.
    '''
    step = 2
    if not request.user.is_authenticated():

        if request.method == 'POST':
            registration_form = CartUserCreationForm(request.POST)
            login_form = AuthForm(data=request.POST)

            chars=string.ascii_lowercase
            password_generation = ''.join(random.choice(chars) for x in range(6))

            if registration_form.is_valid():
                cd = registration_form.cleaned_data
                activate_code = unique_str_generator(30)
                user = User(
                    email=cd['email'],
                    mobile_phone=cd['mobile_phone'],
                    name=cd['name'],
                    regular_customer=cd['regular_customer'],
                    is_active=True,
                    activate_code=activate_code,
                )
                user.set_password(password_generation)
                user.save()

                user.backend='django.contrib.auth.backends.ModelBackend'
                login(request, user)

                send_mail(
                    u'Регистрация на сайте Revert',
                    get_template('cart_registration_message.txt').render(
                        Context({
                            'name': cd['name'],
                            'activate': activate_code,
                            'site': settings.SITE_URL,
                            'password': password_generation,
                        })
                    ),
                    'hooper_spd@mail.ru',
                    [cd['email'], ]
                )

                return HttpResponseRedirect('/cart/ordering/')

    login_form = AuthForm()
    registration_form = CartUserCreationForm()

    return render(request, 'user_accuracy.jade', {
        'step': step,
        'login_form': login_form,
        'registration_form': registration_form,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/cart/','title':'Корзина'}
        ]
    })

def ordering(request):
    step = 3

    price_sum = ''
    uri = request.COOKIES.get('cart', '')

    while True:
        dec = urllib2.unquote(uri)
        if dec == uri:
            break
        uri = dec
    if uri:
        json_cart = json.loads(uri)
        price_sum = sum([item['price'] * item['amount'] for item in json_cart['items']] + [item['price'] * item['amount'] for item in json_cart['graffiti']])

    other_addresses = Address.objects.filter(address_default__id=request.user.id, permanent_address=False)

    try:
        permanent_address = Address.objects.get(address_default__id=request.user.id, permanent_address=True)
    except:
        permanent_address = ''

    if request.method == 'POST':
        form = AddAddressForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(ordering))
    else:
        form = AddAddressForm(initial=
            {'address_default': request.user},
        )

    return render(request, 'ordering.jade', {
        'step': step,
        'form': form,
        'other_addresses': other_addresses,
        'permanent_address': permanent_address,
        'price_sum': price_sum,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/cart/','title':'Корзина'}
        ]
    })

def order(request):
    uri = request.COOKIES.get('cart', '')
    order = Orders(
        user=request.user,
    )
    order.save()

    while True:
        dec = urllib2.unquote(uri)
        if dec == uri:
            break
        uri = dec

    if uri:
        json_cart = json.loads(uri)

        item_list = [item for item in json_cart['items']]

        # Собираем все цвета заказанного граффити в дикт и потом запиливаем в поле color.
        graffiti_list = [item for item in json_cart['graffiti']]
        # graffiti_ids = [item['id'] for item in json_cart['graffiti']]
        try:
            graffiti_colors = [item['color'] for item in json_cart['graffiti'][0]['specs']]
            graffiti_amount = [item['amount'] for item in json_cart['graffiti'][0]['specs']]
        except IndexError:
            graffiti_colors = []
            graffiti_amount = []

        graffiti_colors_obj = ColorPaint.objects.filter(title__in=graffiti_colors)

        graffiti_typle = []
        grafifiti_spec = {}
        for i, item in enumerate(graffiti_colors_obj):
            grafifiti_spec['color'] = item.title
            grafifiti_spec['hex_code'] = item.hex_code
            grafifiti_spec['paint_number'] = item.paint_number
            grafifiti_spec['amount'] = graffiti_amount[i]
            graffiti_typle.append(grafifiti_spec)
            grafifiti_spec = {}

        # Формируем сумму заказа.
        price_sum = sum([item['price'] * item['amount'] for item in json_cart['items']] + [item['price'] * item['amount'] for item in json_cart['graffiti']])

        try:
            address = request.user.address_set.get(permanent_address=True)
        except ObjectDoesNotExist:
            address = 'NA'

        for item in item_list:
            itemq = Item.objects.get(id=item['id'])
            order_item = OrderItem(
                order=order,
                item=Item.objects.get(id=item['id']),
                spec=Specification.objects.get(id=item['spec']),
                address=address,
                count=item['amount'],
                color=item['color'],
                size=item['size'],
            )
            if item['price'] != 0:
                itemq.sold_count+=1
                itemq.save()
                order_item.save()

        for item in graffiti_list:
            itemq = Item.objects.get(id=item['id'])

            for spec in item['specs']:
                order_item, created = OrderItem.objects.get_or_create(
                    order=order,
                    item=Item.objects.get(id=item['id']),
                    count=item['amount'],
                )
                if created:
                    order_item.spec = Specification.objects.get(id=spec['spec'])
                    order_item.color = graffiti_typle
                    order_item.size = spec['size']
                    order_item.save()

            if item['price'] != 0:
                itemq.sold_count+=1
                itemq.save()
                order_item.save()

        mail = EmailMultiAlternatives(
            u'Заказ на сайте Revert',
            get_template('order_list.html').render(
                Context({
                    'item_list': item_list,
                    "graffiti_colors": graffiti_colors_obj,
                    'order_item': order_item,
                    'order': order,
                    'price_sum': price_sum,
                    'site': settings.SITE_URL,
                })
            ),
            'artem@revert.pro',
            [request.user.email, ]
        )
        mail.content_subtype = "html"

        mail_html = get_template('order_list.html').render(
                Context({
                    'item_list': item_list,
                    'order_item': order_item,
                    'order': order,
                    'price_sum': price_sum,
                    'site': settings.SITE_URL,
                })
            )

        f = open('pomoika.html', 'w+')
        f.write(mail_html.encode('utf-8'))
        f.close()

        mail.send()

        response = HttpResponseRedirect('/cart/thx/%s/' % order.id)
        response.set_cookie('cart', '')
    return response


@login_required
def cart_permanent_address(request, address_id):
    address = get_object_or_404(Address, id=address_id)
    if address.address_default == request.user:
        try:
            permanent_address = request.user.address_set.get(permanent_address=True)
            permanent_address.permanent_address = False

            permanent_address.save()
            address.permanent_address = True
            address.save()
        except:
            address.permanent_address = True
            address.save()

    return HttpResponseRedirect(reverse(ordering))

def thx(request, order_id):
    order = get_object_or_404(Orders, id=order_id)

    return render(request, 'thx.html', {
        'order': order,
    })

