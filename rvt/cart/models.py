# -*- coding: utf-8 -*-
from django.db import models

class FreeShipping(models.Model):
    min_ballon_count = models.IntegerField(u'Количество баллонов')
    min_order_summ = models.IntegerField(u'Минимальная сумма заказа')
