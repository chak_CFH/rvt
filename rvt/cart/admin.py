# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.cart.models import FreeShipping

class FreeShippingAdmin(admin.ModelAdmin):
	list_display = ('min_ballon_count', 'min_order_summ',)

admin.site.register(FreeShipping, FreeShippingAdmin)