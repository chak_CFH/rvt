Здравствуйте. Вас пиветствует Revert Boardshop.

От Вас поступил заказ:

    Номер заказа: {{ order_item.order.order_number }}

    Товар:
        {% for item in order.orderitem_set.all %}
            Товар: {{ item.item.name }} {{ item.item.brand }} {{ item.item.item_model }}
            Количество: {{ item.count }}
            Цвет: {{ item.color }}
            Размер: {{ item.size }}
            Цена: {{ item.item.price }}
        {% endfor %}

Общая сумма заказа составляет: {{ price_sum }} рублей.

Спасибо за Ваш заказ!