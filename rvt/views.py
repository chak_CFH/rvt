# -*- coding: utf-8 -*-
from django.shortcuts import render
from rvt.accounts.forms import AuthForm
from django.contrib.sites.models import Site
from rvt.slidertext.models import SliderText
from rvt.comments.models import Comment
from rvt.items.models import Item
from rvt.maintext.models import MainText
from django.views.decorators.cache import cache_page

# @cache_page(60 * 15)
def home(request):
    form = AuthForm
    comments = Comment.objects.order_by('?')[:2]
    slider_text = SliderText.objects.filter(show=True)
    maintext = MainText.objects.latest('id')

    discounts = Item.objects.filter(discount__isnull=False).order_by('?')[:20]
    last_added = Item.objects.all().order_by('-date_add')[:20]
    return render(request, 'main.jade', {
        'form': form,
        'comments': comments,
        'slider': slider_text,
        'discounts': discounts,
        'last_added': last_added,
        'page_name_class': 'page-main',
        'maintext': maintext,
    })

def sitemap(request):
    return render(request, 'sitemap.xml', {}, content_type="application/xhtml+xml")