# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'rvt.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'rvt.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name
from rvt.orders.models import Orders

class MyModule(Dashboard):

    def __init__(self, **kwargs):
        Dashboard.__init__(self, **kwargs)

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        # self.message = Orders.objects.all().count()
        # append a link list module for "quick links"

        # self.children.append(modules.LinkList(
        #     _('Quick links'),
        #     layout='inline',
        #     draggable=False,
        #     deletable=False,
        #     collapsible=False,
        #     children=[
        #         [_('Return to site'), '/'],
        #         [_('Change password'),
        #          reverse('%s:password_change' % site_name)],
        #         [_('Log out'), reverse('%s:logout' % site_name)],
        #     ]
        # ))

        self.children.append(
            modules.ModelList(
                title = u'Клиенты',
                models=(
                    'rvt.accounts.models.*',
                    'rvt.addresses.models.*',
                ),
            )
        )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Каталог',
        #         models=(
        #             'rvt.items.models.*',
        #         ),
        #     )
        # )
        self.children.append(
            modules.ModelList(
                title = u'Каталог',
                template = 'my_blocks/catalog.html',
            )
        )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Вопрос-Ответ',
        #         models=(
        #             'rvt.faq.models.*',
        #         ),
        #     )
        # )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Отызвы покупателей',
        #         models=(
        #             'rvt.comments.models.*',
        #         ),
        #     )
        # )
        self.children.append(
            modules.ModelList(
                title = u'Адреса пользователей',
                models=(
                    'rvt.addresses.models.*',
                ),
            )
        )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Контакты',
        #         models=(
        #             'rvt.contacts.models.*',
        #         ),
        #     )
        # )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Новости',
        #         models=(
        #             'rvt.news.models.*',
        #         ),
        #     )
        # )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Обзоры',
        #         models=(
        #             'rvt.reviews.models.*',
        #         ),
        #     )
        # )
        # self.children.append(
        #     modules.ModelList(
        #         title = u'Слайдер',
        #         models=(
        #             'rvt.slidertext.models.*',
        #         ),
        #     )
        # )
        self.children.append(
            modules.ModelList(
                title = u'Метки(Тэги)',
                models=(
                    'taggit.models.*',
                ),
            )
        )
        self.children.append(
            modules.ModelList(
                title = u'Заказы',
                template = 'my_blocks/orders.html',
                models=(
                    'rvt.orders.models.OrderStatus',
                    'rvt.orders.models.OrderItem',
                ),
                children=[
                    {u'new_orders': Orders.objects.filter(status__status_name=u'Новый').count()},
                    {u'inwork_orders': Orders.objects.filter(status__status_name=u'В работе').count()},
                    {u'finished_orders': Orders.objects.filter(status__status_name=u'Выполненные').count()},
                ]
            )
        )
        self.children.append(
            modules.ModelList(
                title = u'Парсер',
                template = 'my_blocks/parser.html',
                # message = kwargs.get('message', ''),
                models=(
                    'rvt.csvimport.models.*',
                ),
                children=[
                    ['test', Orders.objects.count()],
                    # [_('Return to site'), '/'],
                    # [_('Change password'),
                    #  reverse('%s:password_change' % site_name)],
                    # [_('Log out'), reverse('%s:logout' % site_name)],
                ]
            )
        )
        self.children.append(
            modules.ModelList(
                title = u'Модерация',
                template = 'my_blocks/apps.html',
            )
        )

        # self.children.append(MyModuleTest(title=u"Приветствие", message = u'Привет!'))

        # append an app list module for "Applications"
        # self.children.append(modules.AppList(
        #     _('Applications'),
        #     exclude=('django.contrib.*',),
        # ))

        # append an app list module for "Administration"
        self.children.append(modules.AppList(
            _('Administration'),
            models=('django.contrib.*',),
        ))

class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for rvt.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu


class CustomMenu(Menu):
    """
    Custom Menu for rvt admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.MenuItem(u'Вернуться на сайт', '/'),
            # items.Bookmarks(),
            # items.AppList(
            #     _('Applications'),
            #     exclude=('django.contrib.*',)
            # ),
            # items.AppList(
            #     _('Administration'),
            #     models=('django.contrib.*',)
            # ),
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)