# Django settings for rvt project.
import os

PATH = os.path.abspath(os.path.dirname(__file__))
CURRENT_PATH = os.path.abspath(os.path.curdir)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'revert',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']

SITE_URL = 'http://107.170.1.109' # 'http://revert.com'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru-ru'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(CURRENT_PATH, 'www', 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(CURRENT_PATH, 'www', 'static')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PATH, 'static'),
    os.path.join(CURRENT_PATH, 'www', 'media'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'less.finders.LessFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'r7^r_+!p*vtur+c!kvxi=rpt#pv7*thyyt--p5=tp7)bmj&0)h'

# OLD!!!!!!!!!!
# # List of callables that know how to import templates from various sources.
# TEMPLATE_LOADERS = (
#     'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.filesystem.Loader',
#     #     'django.template.loaders.eggs.Loader',
# )

TEMPLATE_LOADERS = (
    ('pyjade.ext.django.Loader',(
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.debug",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    "rvt.context_processors.current_site.site",
    "rvt.context_processors.footer_brand.footbrands_context",
)

ROOT_URLCONF = 'rvt.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'rvt.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PATH, 'templates'),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.flatpages',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'nested_inlines',
    'filebrowser',
    'django.contrib.admin',
    # 'rvt.catalog',
    'rvt.accounts',
    'taggit',
    'rvt.addresses',
    'rvt.reviews',
    'rvt.faq',
    'rvt.django_ffiler',
    'less',
    'tinymce',
    'pytils',
    'rvt.slidertext',
    'rvt.contacts',
    'sorl.thumbnail',
    'rvt.news',
    'rvt.items',
    'rvt.orders',
    'menu',
    'awesome_avatar',
    'rvt.comments',
    'rvt.brands',
    'rvt.cart',
    'rvt.csvimport',
    'paintstore',
#    'south',
#   'adminsortable',
    'djsupervisor',
    'rvt.maintext',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

AUTH_USER_MODEL = 'accounts.User'

EMAIL_HOST = 'smtp.mail.ru'
EMAIL_PORT = 2525
EMAIL_HOST_USER = 'hooper_spd@mail.ru'
EMAIL_HOST_PASSWORD = 'q12w12'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'hooper_spd@mail.ru'

TINYMCE_DEFAULT_CONFIG={
    'theme' : "advanced",
    'language' : 'ru',
    # plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager,filemanager",
    'plugins' : "table,paste",
    'theme_advanced_buttons3' : "tablecontrols,|,justifyleft,justifycenter,justifyright,|,code",
    'theme_advanced_buttons1' : "styleselect,formatselect,fontselect,fontsizeselect",
    'theme_advanced_buttons2' : "bullist,numlist,|,link,unlink,anchor,image,|,bold,italic,underline,|,forecolor,backcolor,|,cut,copy,paste,pastetext,pasteword,|,undo,redo,|,link,unlink,cleanup",
    'theme_advanced_toolbar_location' : "top",
    'theme_advanced_toolbar_align' : "left",
    'theme_advanced_statusbar_location' : "bottom",
    'theme_advanced_resizing' : True,
    'table_default_cellpadding': 2,
    'table_default_cellspacing': 2,
    'cleanup_on_startup' : False,
    'cleanup' : True, # False,
    'paste_auto_cleanup_on_paste' : True, #False,
    'paste_remove_styles' : True, ##
    'paste_remove_styles_if_webkit': True, ##
    'paste_strip_class_attributes': True, ##
    'paste_text_sticky': True, ##
    'paste_text_sticky_default': True, ##
    'paste_block_drop' : False,
    'paste_remove_spans' : False,
    'paste_strip_class_attributes' : False,
    'paste_retain_style_properties' : "",
    'forced_root_block' : False,
    'force_br_newlines' : False,
    'force_p_newlines' : True,
    'remove_linebreaks' : False,
    'convert_newlines_to_brs' : False,
    'inline_styles' : False,
    'relative_urls' : False,
    'formats' : {
        # 'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-left'},
        # 'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-center'},
        # 'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-right'},
        # 'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', 'classes' : 'align-justify'},
        'float' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,img', 'classes' : 'left'},
        'float' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,img', 'classes' : 'center'},
        'float' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,img', 'classes' : 'right'},
        'float' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,img', 'classes' : 'justify'},
        'margin' : {'selector' : 'table', 'classes' : 'auto'},
        'strikethrough' : {'inline' : 'del'},
        'italic' : {'inline' : 'em'},
        'bold' : {'inline' : 'strong'},
        'underline' : {'inline' : 'u'},
    },
    'pagebreak_separator' : ""
}

LOGIN_REDIRECT_URL = '/'

# ADMIN_TOOLS_MENU = 'rvt.menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'rvt.dashboard.MyModule' # 'rvt.dashboard.CustomIndexDashboard'
# ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'rvt.dashboard.MyModule'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'rvt.dashboard.CustomAppIndexDashboard'
ADMIN_TOOLS_THEMING_CSS = 'css/theming.css'
