# -*- coding: utf-8 -*-
from django.db import models
from sorl.thumbnail import ImageField
'''
class RandomManager(models.Manager):
    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]
'''
class Comment(models.Model):
    author = models.CharField(u'Автор', max_length=50)
    vklink = models.CharField(u'Ссылка на автора(Вконтакте).', max_length=255, blank=True, null=True)
    content = models.TextField(u'Текст отзыва')
    image = ImageField(u'Фото автора', upload_to='comments_media/')
    date = models.DateTimeField(u'Дата добавления отзыва')
    #randoms = RandomManager()

    class Meta:
        verbose_name = u'Отзывы покупателей'
        verbose_name_plural = u'Отзывы покупателей'

    def __unicode__(self):
        return self.author