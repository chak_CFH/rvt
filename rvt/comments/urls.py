# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from rvt.comments.views import comments_list

urlpatterns = patterns('',
	url(r'^$', comments_list, name='comments_list'),
	# url(r'^(\w+)/$', review_detail, name='review_detail'),
)