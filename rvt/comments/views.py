# -*- coding: utf-8 -*-
from django.shortcuts import render
from rvt.comments.models import *

def comments_list(request):
    comments = Comment.objects.all()

    return render(request, 'comments_list.jade', {
        'comments': comments,
    })
