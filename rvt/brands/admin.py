# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.brands.models import Brand
from sorl.thumbnail import ImageField
from rvt.img_sorl import AdminImageWidget

class BrandAdmin(admin.ModelAdmin):
    list_display = ('image_preview','title', 'url', )
    search_fields = ['title', ]
    ordering = ['-title', ]
    formfield_overrides = {
        ImageField: {'widget': AdminImageWidget},
    }

admin.site.register(Brand, BrandAdmin)