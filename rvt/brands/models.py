# -*- coding: utf-8 -*-
from django.db import models
from sorl.thumbnail import ImageField

class Brand (models.Model):
    title = models.CharField(u'Бренд', max_length=50)
    image = ImageField(u'Изображение', upload_to='images/brands/')
    url = models.CharField(u'Ссылка', max_length=50)

    def image_preview(self):
        if self.image:
            return '<img src="%s" width="100">' % self.image.url
        else:
            return '(none)'
    image_preview.short_description = u'Изображение'
    image_preview.allow_tags = True
    
    def __unicode__(self):
        return self.title