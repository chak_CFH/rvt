# -*- coding: utf-8 -*-
import re
from django.db import models
from django.db.models import Sum
from tinymce.models import HTMLField
from sorl.thumbnail import ImageField
from pytils import translit
from paintstore.fields import ColorPickerField
from sorl.thumbnail.shortcuts import get_thumbnail

#тип размера, например: для одежды, для обуви
class Category(models.Model):
    name = models.CharField(u'Название категории', max_length=60, db_index=True)
    price_min = models.IntegerField(u'Минимальная цена', blank=True, null=True, db_column='price_range')
    price_max = models.IntegerField(u'Максимальная цена', blank=True, null=True)
    slug = models.CharField(u'Название категории по-английски(ЧПУ)', max_length=60, db_index=True)

    class Meta:
        verbose_name = u'Категория товара'
        verbose_name_plural = u'Категория товара'

    def __unicode__(self):
        return self.name

class Brand(models.Model):
    ''' Список брэндов '''
    name = models.CharField(u'Название брэнда', max_length=60, db_index=True)

    class Meta:
        verbose_name = u'Брэнд товара'
        verbose_name_plural = u'Брэнд товара'

    def __unicode__(self):
        return self.name

class Season(models.Model):
    ''' Сезон товара '''
    name = models.CharField(u'Сезон', max_length=60, db_index=True)

    class Meta:
        verbose_name = u'Сезон'
        verbose_name_plural = u'Сезон'

    def __unicode__(self):
        return self.name

class SizeType(models.Model):
    title = models.CharField(u'Тип товара для размера', max_length=255)

    class Meta:
        verbose_name = u'Тип товара и размер'
        verbose_name_plural = u'Тип товара и размер'

    def __unicode__(self):
        return self.title

# тут все понятно
class Color(models.Model):
    title = models.CharField(u'Название цвета', max_length=255, db_index=True)
    hex_code = ColorPickerField(u'Код цвета', db_index=True)

    class Meta:
        verbose_name = u'Цвет товара'
        verbose_name_plural = u'Цвет товара'

    def __unicode__(self):
        return self.title

class ColorPaint(models.Model):
    ''' Цвет краски '''
    title = models.CharField(u'Название цвета', max_length=255, db_index=True)
    hex_code = ColorPickerField(u'Код цвета', db_index=True)
    paint_number = models.CharField(u'Код краски', max_length=30, blank=True, null=True, db_index=True)

    class Meta:
        verbose_name = u'Цвет карски'
        verbose_name_plural = u'Цвет карски'

    def __unicode__(self):
        return self.title

# значения для размера, например: XXL, S, M или 40, 42, 45
class SizeVal(models.Model):
    size_type = models.ForeignKey(SizeType, blank=True, null=True)
    value = models.CharField(max_length=255)

    class Meta:
        verbose_name = u'Значения размеров'
        verbose_name_plural = u'Значения размеров'

    def __unicode__(self):
        return self.value

# Собственно, сам товар
SEX = (
    (u'М', u'М'),
    (u'Ж', u'Ж'),
    (u'Д', u'Д'),
)
class Item(models.Model):
    category = models.ForeignKey(Category, verbose_name=u'Категория товара', db_index=True)
    name = models.CharField(u'Название товара', max_length=255, db_index=True)
    brand = models.ForeignKey(Brand, verbose_name=u'Брэнд', db_index=True)
    item_model = models.CharField(u'Модель товара', max_length=128, db_index=True)
    item_model_slug = models.CharField(
        u'Модель товара(ЧПУ)',
        max_length=128,
        help_text=u'Заполняется автоматически после сохранения',
        blank=True,
        null=True,
        unique=True,
        editable=False,
        db_index=True,
    )
    sex = models.CharField(u'Пол', max_length=2, choices=SEX, blank=True)
    date_add = models.DateTimeField(u'Дата добавления товара', auto_now=True)
    description = HTMLField(u'Описание товара', blank=True)
    season = models.ForeignKey(Season, verbose_name=u'Сезон', blank=True, null=True)
    sold_count = models.IntegerField(u'Кол-во проданого')
    discount = models.IntegerField(u'Скидка', blank=True, null=True)
    price = models.IntegerField(u'Цена')
    old_price = models.IntegerField(u'Старая цена', blank=True, null=True)
    # woman = models.BooleanField(u'Женский раздел', blank=True)

    class Meta:
        verbose_name = u'Товар'
        verbose_name_plural = u'Товар'

    def __unicode__(self):
        return '%s %s %s(%s)' % (self.name, self.brand, self.item_model, self.category)

    def is_graffiti(self):
        if self.category.slug in ['graffiti_paint','graffiti_markers',
                                  'graffiti_refill','graffiti_accessories', ]:
            return True
        return False

    def image_oreder_preview(self):
        image_prev = ''
        for item in self.specification_set.all():
            for image in item.specificationimages_set.all():
                return image

    def image_preview(self):
        image_prev = ''
        for item in self.specification_set.all():
            for image in item.specificationimages_set.all():
                image_prev = '<img src="%s">' % unicode(get_thumbnail(image.image, '100x100').url)
            if not image_prev:
                return '(Нет изображения)'
            else:
                return image_prev
    image_preview.short_description = u'Изображение'
    image_preview.allow_tags = True

    def image_mail_preview(self):
        image_prev = ''
        for item in self.specification_set.all():
            for image in item.specificationimages_set.all():
                image_prev = unicode(get_thumbnail(image.image, '100x100').url)
            if not image_prev:
                return '(Нет изображения)'
            else:
                return image_prev
    image_mail_preview.short_description = u'Изображение'
    image_mail_preview.allow_tags = True

    def get_desc(self):
        if self.description:
            return True
        return False
    get_desc.short_description = u'Описание товара'
    get_desc.boolean = True

    def save(self, *args, **kwargs):
        ''' Переопределенный метод сэйв с сохранением транслита модели товара для ЧПУ. '''
        translit_slug = translit.translify(self.item_model.replace(' ', '_'))
        self.item_model_slug = re.sub(u"[^a-zA-Z0-9\_\s]", "", translit_slug)
        super(Item, self).save(*args, **kwargs)

# связь товара цвета и размеров (спецификация)
class Specification(models.Model):
    item = models.ForeignKey(Item, verbose_name=u'Товар')
    color = models.ForeignKey(Color, verbose_name=u'Цвет товара', blank=True, null=True)
    paint_color = models.ForeignKey(ColorPaint, verbose_name=u'Цвет краски', blank=True, null=True)
    size_list = models.ManyToManyField(SizeVal, verbose_name=u'Размер/Объем')

    class Meta:
        verbose_name = u'Спецификации товара'
        verbose_name_plural = u'Спецификации товара'

    def __unicode__(self):
        return u'%s' % self.item

    def get_thumbnail(self):
        img = self.specificationimages_set.all()[0].image.url
        # img_resize_url = unicode(get_thumbnail(img, '200x200').url)
        return img

    def is_graffiti(self):
        if self.item.category.slug in ['graffiti_paint','graffiti_markers',
                                  'graffiti_refill','graffiti_accessories', ]:
            return True
        return False

class SpecificationImages(models.Model):
    image = models.ImageField(upload_to='sub_media/')
    specs = models.ForeignKey(Specification, verbose_name=u'Спецификации')

    def __unicode__(self):
        return u'Изображение для товара %s' % self.specs.item

    # def image(self):
    #     return '<img src="/media/%s" width="200px" heigth=""200px/>' % self.image
    # image.allow_tags = True
    # image.short_description = u'Изображение товара'

    def get_thumbnail_html(self):
        img = self.image
        img_resize_url = unicode(get_thumbnail(img, '200x200').url)
        html = '<a class="image-picker" href="/admin/items/specificationimages/%s/"><img src="%s"/></a>'
        return html % (self.id, img_resize_url)
    get_thumbnail_html.allow_tags = True

    class Meta:
        verbose_name = u'Изображения товара'
        verbose_name_plural = u'Изображения товара'

