# -*- coding: utf-8 -*-
import json
import urllib2
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from rvt.items.models import *
from rvt.items.forms import (ClothesFilterForm, EquipmentFilterForm, ShoesFilterForm,
                               OpticsFilterForm, AccessoriesFilterForm, FilterForm, GraffityFilterForm)

from django.views.decorators.cache import cache_page
from django.db.models import Q

# @cache_page(60 * 15)
def catalog_list(request):
    category_slug = request.path.split('/')[2]
    category_name = Category.objects.filter(slug=category_slug)

    if category_name:
        category_name = category_name[0]
        category_name_slug = category_name.slug
    else:
        category_name = ''
        category_name_slug = ''

    form = FilterForm(request.GET)

    if category_slug in ['mans_equipment', 'mans_clothes','mans_shoes','mans_snowboard_wear',
    					'mans_accessories','mans_optics','women_equipment','women_clothes',
     					'women_shoes','women_snowboard_wear','women_accessories','women_optics',
     					'kids_equipment','kids_clothes','kids_shoes','kids_snowboard_wear',
     					'kids_accessories','kids_optics', ]:

        form = ClothesFilterForm(request.GET)

    # if category_slug in ['graffiti_paint','graffiti_markers',
    #  					'graffiti_refill','graffiti_accessories', ]:
    if category_slug in ['graffiti_paint', ]:
        form = GraffityFilterForm(request.GET)

    if category_slug in ['mans_equipment','women_equipment','kids_equipment']:
        form = EquipmentFilterForm(request.GET)

    if category_slug in ['mans_shoes', 'womans_shoes', 'kids_shoes']:
        form = ShoesFilterForm(request.GET)

    if category_slug in ['mans_accessories', 'women_accessories', 'kids_accessories']:
        form = AccessoriesFilterForm(request.GET)

    if category_slug in ['mans_optics', 'women_optics', 'kids_optics']:
        form = OpticsFilterForm(request.GET)
    # if not category_slug:
    #     form = FilterForm(request.GET)

    color = request.GET.getlist('color', '')
    size = request.GET.getlist('size', '')
    price_min = request.GET.get('price_min', '') if request.GET.get('price_min', '') != '0' else None
    price_max = request.GET.get('price_max', '') if request.GET.get('price_max', '') != '0' else None
    brand = request.GET.getlist('brand', '')
    season = request.GET.get('season', '')
    
    # if category_slug in [
    #     'women_equipment',
    #     'women_clothes',
    #     'women_shoes',
    #     'women_snowboard_wear',
    #     'women_accessories',
    #     'women_optics',]:
    #
    #     catalog = Item.objects.filter(Q(category__slug=category_slug) | Q(woman=True)).order_by('-date_add').distinct()
    if category_slug:
        catalog = Item.objects.filter(category__slug=category_slug).order_by('-date_add').distinct()
    else:
        catalog = Item.objects.all().order_by('-date_add')

    if price_min:
        catalog = catalog.filter(price__gte=price_min)
    if price_max:
        catalog = catalog.filter(price__lte=price_max)
    if color:
        catalog = catalog.filter(specification__color__id__in=color)
    if size:
        catalog = catalog.filter(specification__size_list__value__in=size)
    if brand:
        catalog = catalog.filter(brand__name__in=brand)
    if season:
        catalog = catalog.filter(season__in=season)

    template = 'catalog_list.jade'

    if request.is_ajax():
        template = 'catalog-item.jade'

    return render(request, template, {
        'catalog': catalog,
        'category_name': category_name,
        'form': form,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/catalog','title':'Каталог'},
            {'url':'/catalog/%s' % category_name_slug,'title':category_name}
        ]
    })

def catalog_detail(request, item_model_slug):
    item = get_object_or_404(Item, item_model_slug=item_model_slug)
    uri = request.COOKIES.get('last_views', '')

    # while True:
    #     dec = urllib2.unquote(uri)
    #     if dec == uri:
    #         break
    #     uri = dec
    #
    # if uri:
    #     json_last_views = json.loads(uri)

    rec_item_list = Item.objects.filter(
        category=item.category.id,
        price__lte=item.category.price_max,
        price__gte=item.category.price_min
        ).exclude(
        id=item.id,
        )

    response = HttpResponse()
    response.delete_cookie('last_views')

    response = HttpResponse()
    response = render(request, 'catalog_detail.jade', {
        'rec_item_list': rec_item_list,
        'item': item,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/catalog','title':'Каталог'},
            {'url':'/catalog/%s' % item.category.slug,'title':item.category},
            {'url':'/catalog/%s/%s' % (item.category.slug, item.item_model_slug), 'title': item.item_model},
        ]
    })
    last_views = []
    if not request.COOKIES.get('last_views', None):
        last_views.append(item.id)
    else:
        last_views = []
        last_views.append(request.COOKIES.get('last_views'))
        last_views.append(item.id)

    # last_views = []
    # for view in request.COOKIES.get('last_views'):
    #     last_views.append(view)
    #     response.set_cookie('last_views', view)
    #
    # qwe = [1, 2, 3, 4]


    response.set_cookie('last_views', last_views)
    qwe = request.COOKIES.get('last_views')

    return response

def catalog_item_quick_view(request, item_model_slug):
    item = get_object_or_404(Item, item_model_slug=item_model_slug)
    return render(request, 'catalog-item-body.jade', {
        'item': item,
    })