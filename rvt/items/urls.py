# -*- coding: utf-8 -*
from django.conf.urls import patterns, include, url
from rvt.items.views import catalog_list, catalog_detail, catalog_item_quick_view

urlpatterns = patterns('',
    url(r'catalog', include(patterns('',
        # Одежда
        url(r'/mans_clothes/$', catalog_list, name='mans_catalog'),
        url(r'/mans_clothes/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/women_clothes/$', catalog_list, name='women_clothes'),
        url(r'/women_clothes/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/kids_clothes/$', catalog_list, name='kids_clothes'),
        url(r'/kids_clothes/(\w+)/$', catalog_detail, name='catalog_detail'),

        # Оборудование
        url(r'/mans_equipment/$', catalog_list, name='mans_equipment'),
        url(r'/mans_equipment/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/women_equipment/$', catalog_list, name='women_equipment'),
        url(r'/women_equipment/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/kids_equipment/$', catalog_list, name='kids_equipment'),
        url(r'/kids_equipment/(\w+)/$', catalog_detail, name='catalog_detail'),

        # Обувь
        url(r'/mans_shoes/$', catalog_list, name='mans_shoes'),
        url(r'/mans_shoes/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/womans_shoes/$', catalog_list, name='womans_shoes'),
        url(r'/womans_shoes/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/kids_shoes/$', catalog_list, name='kids_shoes'),
        url(r'/kids_shoes/(\w+)/$', catalog_detail, name='catalog_detail'),

        # Одежда для сноуборда
        url(r'/mans_snowboard_wear/$', catalog_list, name='mans_snowboard_wear'),
        url(r'/mans_snowboard_wear/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/women_snowboard_wear/$', catalog_list, name='women_snowboard_wear'),
        url(r'/women_snowboard_wear/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/kids_snowboard_wear/$', catalog_list, name='kids_snowboard_wear'),
        url(r'/kids_snowboard_wear/(\w+)/$', catalog_detail, name='catalog_detail'),

        # Аксесуары
        url(r'/mans_accessories/$', catalog_list, name='mans_accessories'),
        url(r'/mans_accessories/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/women_accessories/$', catalog_list, name='women_accessories'),
        url(r'/women_accessories/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/kids_accessories/$', catalog_list, name='kids_accessories'),
        url(r'/kids_accessories/(\w+)/$', catalog_detail, name='catalog_detail'),

        # Оптика
        url(r'/mans_optics/$', catalog_list, name='mans_optics'),
        url(r'/mans_optics/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/women_optics/$', catalog_list, name='women_optics'),
        url(r'/women_optics/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/kids_optics/$', catalog_list, name='kids_optics'),
        url(r'/kids_optics/(\w+)/$', catalog_detail, name='catalog_detail'),

        # Граффити
        url(r'/graffiti_paint/$', catalog_list, name='graffiti_paint'),
        url(r'/graffiti_paint/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/graffiti_markers/$', catalog_list, name='graffiti_markers'),
        url(r'/graffiti_markers/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/graffiti_refill/$', catalog_list, name='graffiti_refill'),
        url(r'/graffiti_refill/(\w+)/$', catalog_detail, name='catalog_detail'),
        url(r'/graffiti_accessories/$', catalog_list, name='graffiti_accessories'),
        url(r'/graffiti_accessories/(\w+)/$', catalog_detail, name='catalog_detail'),

        # Мужские товары

        ## Сноубординг
        # Cноуборды
        url(r'/mans_snowboards/$', catalog_list, name='mans_snowboards'),
        url(r'/mans_snowboards/(\w+)/$', catalog_detail, name='mans_snowboards'),

        # Крепления
        url(r'/mans_bindings/$', catalog_list, name='mans_bindings'),
        url(r'/mans_bindings/(\w+)/$', catalog_detail, name='mans_bindings'),

        # Ботинки
        url(r'/mans_boots/$', catalog_list, name='mans_boots'),
        url(r'/mans_boots/(\w+)/$', catalog_detail, name='mans_boots'),

        # Чехлы для сноуборда
        url(r'/boardcovers/$', catalog_list, name='boardcovers'),
        url(r'/boardcovers/(\w+)/$', catalog_detail, name='boardcovers'),

        # Куртки г/л
        url(r'/mans_ski_jackets/$', catalog_list, name='mans_ski_jackets'),
        url(r'/mans_ski_jackets/(\w+)/$', catalog_detail, name='mans_ski_jackets'),

        # Штаны г/л
        url(r'/mans_boardpants/$', catalog_list, name='mans_boardpants'),
        url(r'/mans_boardpants/(\w+)/$', catalog_detail, name='mans_boardpants'),

        # Термуха и носки
        url(r'/mans_thermal/$', catalog_list, name='mans_thermal'),
        url(r'/mans_thermal/(\w+)/$', catalog_detail, name='mans_thermal'),

        # Термотолстовки
        url(r'/mans_sweatshirt/$', catalog_list, name='mans_sweatshirt'),
        url(r'/mans_sweatshirt/(\w+)/$', catalog_detail, name='mans_sweatshirt'),

        # Защита
        url(r'/mans_boardprotection/$', catalog_list, name='mans_boardprotection'),
        url(r'/mans_boardprotection/(\w+)/$', catalog_detail, name='mans_boardprotection'),

        # Шлемы
        url(r'/mans_boardhelmets/$', catalog_list, name='mans_boardhelmets'),
        url(r'/mans_boardhelmets/(\w+)/$', catalog_detail, name='mans_boardhelmets'),

        # Маски
        url(r'/mans_masks/$', catalog_list, name='mans_masks'),
        url(r'/mans_masks/(\w+)/$', catalog_detail, name='mans_masks'),

        # Варежки перчатки
        url(r'/mans_gloves/$', catalog_list, name='mans_gloves'),
        url(r'/mans_gloves/(\w+)/$', catalog_detail, name='mans_gloves'),

        # Балаклавы
        url(r'/mans_balaklavs/$', catalog_list, name='mans_balaklavs'),
        url(r'/mans_balaklavs/(\w+)/$', catalog_detail, name='mans_balaklavs'),

        # Запчасти для сноуборда
        url(r'/mans_boardgears/$', catalog_list, name='mans_boardgears'),
        url(r'/mans_boardgears/(\w+)/$', catalog_detail, name='mans_boardgears'),

        ## Скейтбординг
        # Лонги
        url(r'/longboards/$', catalog_list, name='longboards'),
        url(r'/longboards/(\w+)/$', catalog_detail, name='longboards'),

        # Скейты в зборе
        url(r'/skateboards/$', catalog_list, name='skateboards'),
        url(r'/skateboards/(\w+)/$', catalog_detail, name='skateboards'),

        # Чехлы
        url(r'/skatecovers/$', catalog_list, name='skatecovers'),
        url(r'/skatecovers/(\w+)/$', catalog_detail, name='skatecovers'),

        # Деки
        url(r'/skatedecs/$', catalog_list, name='skatedecs'),
        url(r'/skatedecs/(\w+)/$', catalog_detail, name='skatedecs'),

        # Лонговые перчатки
        url(r'/longgloves/$', catalog_list, name='longgloves'),
        url(r'/longgloves/(\w+)/$', catalog_detail, name='longgloves'),

        # Шлемы
        url(r'/skatehelmets/$', catalog_list, name='skatehelmets'),
        url(r'/skatehelmets/(\w+)/$', catalog_detail, name='skatehelmets'),

        # Запчасти - скейт, лонг
        url(r'/skategears/$', catalog_list, name='skategears'),
        url(r'/skategears/(\w+)/$', catalog_detail, name='skategears'),


        ## Одежда
        # Куртки
        url(r'/mans_jackets/$', catalog_list, name='mans_jackets'),
        url(r'/mans_jackets/(\w+)/$', catalog_detail, name='mans_jackets'),

        # Джинсы, штаны
        url(r'/mans_jeans/$', catalog_list, name='mans_jeans'),
        url(r'/mans_jeans/(\w+)/$', catalog_detail, name='mans_jeans'),

        # Толстовки, худи
        url(r'/mans_hoodies/$', catalog_list, name='mans_hoodies'),
        url(r'/mans_hoodies/(\w+)/$', catalog_detail, name='mans_hoodies'),

        # Футболки, майки
        url(r'/mans_tshorts/$', catalog_list, name='mans_tshorts'),
        url(r'/mans_tshorts/(\w+)/$', catalog_detail, name='mans_tshorts'),

        # Шорты
        url(r'/mans_shorts/$', catalog_list, name='mans_shorts'),
        url(r'/mans_shorts/(\w+)/$', catalog_detail, name='mans_shorts'),

        # Купальные шорты
        url(r'/mans_swimsuit/$', catalog_list, name='mans_swimsuit'),
        url(r'/mans_swimsuit/(\w+)/$', catalog_detail, name='mans_swimsuit'),

        # Носки briefs
        url(r'/soxs/$', catalog_list, name='soxs'),
        url(r'/soxs/(\w+)/$', catalog_detail, name='soxs'),

        # Трусы
        url(r'/mans_briefs/$', catalog_list, name='mans_briefs'),
        url(r'/mans_briefs/(\w+)/$', catalog_detail, name='mans_briefs'),

        # Кепки
        url(r'/caps/$', catalog_list, name='caps'),
        url(r'/caps/(\w+)/$', catalog_detail, name='caps'),

        # Шапки, шарфы
        url(r'/mans_hats/$', catalog_list, name='mans_hats'),
        url(r'/mans_hats/(\w+)/$', catalog_detail, name='mans_hats'),

        # Кеды
        url(r'/mans_shoes/$', catalog_list, name='mans_shoes'),
        url(r'/mans_shoes/(\w+)/$', catalog_detail, name='mans_shoes'),

        ## Аксессуары
        # Наушники
        url(r'/headphones/$', catalog_list, name='headphones'),
        url(r'/headphones/(\w+)/$', catalog_detail, name='headphones'),

        # Кошельки
        url(r'/purses/$', catalog_list, name='purses'),
        url(r'/purses/(\w+)/$', catalog_detail, name='purses'),

        # Ремни
        url(r'/belts/$', catalog_list, name='belts'),
        url(r'/belts/(\w+)/$', catalog_detail, name='belts'),

        # Чакбадиз
        url(r'/chakbadizs/$', catalog_list, name='chakbadizs'),
        url(r'/chakbadizs/(\w+)/$', catalog_detail, name='chakbadizs'),

        # Фингеры
        url(r'/fingerboards/$', catalog_list, name='fingerboards'),
        url(r'/fingerboards/(\w+)/$', catalog_detail, name='fingerboards'),

        # Часы
        url(r'/clocks/$', catalog_list, name='clocks'),
        url(r'/clocks/(\w+)/$', catalog_detail, name='clocks'),

        # Солнезащитные очки
        url(r'/sunglasses/$', catalog_list, name='sunglasses'),
        url(r'/sunglasses/(\w+)/$', catalog_detail, name='sunglasses'),

        ##########################################################################


        ##########################################################################

        # Женсике товары

        ## Сноубординг
        # Cноуборды
        url(r'/women_snowboards/$', catalog_list, name='women_snowboards'),
        url(r'/women_snowboards/(\w+)/$', catalog_detail, name='women_snowboards'),

        # Крепления
        url(r'/women_bindings/$', catalog_list, name='women_bindings'),
        url(r'/women_bindings/(\w+)/$', catalog_detail, name='women_bindings'),

        # Ботинки
        url(r'/women_boots/$', catalog_list, name='women_boots'),
        url(r'/women_boots/(\w+)/$', catalog_detail, name='women_boots'),

        # Чехлы для сноуборда
        # url(r'/women_boardcovers/$', catalog_list, name='women_boardcovers'),
        # url(r'/women_boardcovers/(\w+)/$', catalog_detail, name='women_boardcovers'),

        # Куртки г/л
        url(r'/women_ski_jackets/$', catalog_list, name='women_ski_jackets'),
        url(r'/women_ski_jackets/(\w+)/$', catalog_detail, name='women_ski_jackets'),

        # Штаны г/л
        url(r'/women_boardpants/$', catalog_list, name='women_boardpants'),
        url(r'/women_boardpants/(\w+)/$', catalog_detail, name='women_boardpants'),

        # Термуха и носки
        url(r'/women_thermal/$', catalog_list, name='women_thermal'),
        url(r'/women_thermal/(\w+)/$', catalog_detail, name='women_thermal'),

        # Термотолстовки
        url(r'/women_sweatshirt/$', catalog_list, name='women_sweatshirt'),
        url(r'/women_sweatshirt/(\w+)/$', catalog_detail, name='women_sweatshirt'),

        # Защита
        url(r'/women_boardprotection/$', catalog_list, name='women_boardprotection'),
        url(r'/women_boardprotection/(\w+)/$', catalog_detail, name='women_boardprotection'),

        # Шлемы
        url(r'/women_boardhelmets/$', catalog_list, name='women_boardhelmets'),
        url(r'/women_boardhelmets/(\w+)/$', catalog_detail, name='women_boardhelmets'),

        # Маски
        url(r'/women_masks/$', catalog_list, name='women_masks'),
        url(r'/women_masks/(\w+)/$', catalog_detail, name='women_masks'),

        # Варежки перчатки
        url(r'/women_gloves/$', catalog_list, name='women_gloves'),
        url(r'/women_gloves/(\w+)/$', catalog_detail, name='women_gloves'),

        # Балаклавы
        url(r'/women_balaklavs/$', catalog_list, name='women_balaklavs'),
        url(r'/women_balaklavs/(\w+)/$', catalog_detail, name='women_balaklavs'),

        # Запчасти для сноуборда
        url(r'/women_boardgears/$', catalog_list, name='women_boardgears'),
        url(r'/women_boardgears/(\w+)/$', catalog_detail, name='women_boardgears'),

        ## Одежда
        # Куртки
        url(r'/women_jackets/$', catalog_list, name='women_jackets'),
        url(r'/women_jackets/(\w+)/$', catalog_detail, name='women_jackets'),

        # Джинсы, штаны
        url(r'/women_jeans/$', catalog_list, name='women_jeans'),
        url(r'/women_jeans/(\w+)/$', catalog_detail, name='women_jeans'),

        # Толстовки, худи
        url(r'/women_hoodies/$', catalog_list, name='women_hoodies'),
        url(r'/women_hoodies/(\w+)/$', catalog_detail, name='women_hoodies'),

        # Футболки, майки
        url(r'/women_tshorts/$', catalog_list, name='women_tshorts'),
        url(r'/women_tshorts/(\w+)/$', catalog_detail, name='women_tshorts'),

        # Шортики
        url(r'/women_shorts/$', catalog_list, name='women_shorts'),
        url(r'/women_shorts/(\w+)/$', catalog_detail, name='women_shorts'),

        # Купальники
        url(r'/women_swimsuit/$', catalog_list, name='women_swimsuit'),
        url(r'/women_swimsuit/(\w+)/$', catalog_detail, name='women_swimsuit'),

        # Трусы
        # url(r'/women_briefs/$', catalog_list, name='women_briefs'),
        # url(r'/women_briefs/(\w+)/$', catalog_detail, name='women_briefs'),

        # Шапки, шарфы
        url(r'/women_hats/$', catalog_list, name='women_hats'),
        url(r'/women_hats/(\w+)/$', catalog_detail, name='women_hats'),

        # Кеды
        url(r'/women_shoes/$', catalog_list, name='women_shoes'),
        url(r'/women_shoes/(\w+)/$', catalog_detail, name='women_shoes'),


        ##########################################################################


        ##########################################################################

        # Детские товары

        ## Сноубординг
        # Cноуборды
        url(r'/kids_snowboards/$', catalog_list, name='kids_snowboards'),
        url(r'/kids_snowboards/(\w+)/$', catalog_detail, name='kids_snowboards'),

        # Крепления
        url(r'/kids_bindings/$', catalog_list, name='kids_bindings'),
        url(r'/kids_bindings/(\w+)/$', catalog_detail, name='kids_bindings'),

        # Ботинки
        url(r'/kids_boots/$', catalog_list, name='kids_boots'),
        url(r'/kids_boots/(\w+)/$', catalog_detail, name='kids_boots'),

        # Чехлы для сноуборда
        # url(r'/kids_boardcovers/$', catalog_list, name='kids_boardcovers'),
        # url(r'/kids_boardcovers/(\w+)/$', catalog_detail, name='kids_boardcovers'),

        # Куртки г/л
        url(r'/kids_ski_jackets/$', catalog_list, name='kids_ski_jackets'),
        url(r'/kids_ski_jackets/(\w+)/$', catalog_detail, name='kids_ski_jackets'),

        # Штаны г/л
        url(r'/kids_boardpants/$', catalog_list, name='kids_boardpants'),
        url(r'/kids_boardpants/(\w+)/$', catalog_detail, name='kids_boardpants'),

        # Комбинезоны
        url(r'/kids_coveralls/$', catalog_list, name='kids_coveralls'),
        url(r'/kids_coveralls/(\w+)/$', catalog_detail, name='kids_coveralls'),

        # Термуха и носки
        url(r'/kids_thermal/$', catalog_list, name='kids_thermal'),
        url(r'/kids_thermal/(\w+)/$', catalog_detail, name='kids_thermal'),

        # Термотолстовки
        url(r'/kids_sweatshirt/$', catalog_list, name='kids_sweatshirt'),
        url(r'/kids_sweatshirt/(\w+)/$', catalog_detail, name='kids_sweatshirt'),

        # Защита
        url(r'/kids_boardprotection/$', catalog_list, name='kids_boardprotection'),
        url(r'/kids_boardprotection/(\w+)/$', catalog_detail, name='kids_boardprotection'),

        # Шлемы
        # url(r'/kids_boardhelmets/$', catalog_list, name='kids_boardhelmets'),
        # url(r'/kids_boardhelmets/(\w+)/$', catalog_detail, name='kids_boardhelmets'),

        # Маски
        # url(r'/kids_masks/$', catalog_list, name='kids_masks'),
        # url(r'/kids_masks/(\w+)/$', catalog_detail, name='kids_masks'),

        # Варежки перчатки
        url(r'/kids_gloves/$', catalog_list, name='kids_gloves'),
        url(r'/kids_gloves/(\w+)/$', catalog_detail, name='kids_gloves'),

        # Балаклавы
        url(r'/kids_balaklavs/$', catalog_list, name='kids_balaklavs'),
        url(r'/kids_balaklavs/(\w+)/$', catalog_detail, name='kids_balaklavs'),

        # Запчасти для сноуборда
        # url(r'/kids_boardgears/$', catalog_list, name='kids_boardgears'),
        # url(r'/kids_boardgears/(\w+)/$', catalog_detail, name='kids_boardgears'),

        ## Одежда
        # Куртки
        url(r'/kids_jackets/$', catalog_list, name='kids_jackets'),
        url(r'/kids_jackets/(\w+)/$', catalog_detail, name='kids_jackets'),

        # Джинсы, штаны
        url(r'/kids_jeans/$', catalog_list, name='kids_jeans'),
        url(r'/kids_jeans/(\w+)/$', catalog_detail, name='kids_jeans'),

        # Толстовки, худи
        url(r'/kids_hoodies/$', catalog_list, name='kids_hoodies'),
        url(r'/kids_hoodies/(\w+)/$', catalog_detail, name='kids_hoodies'),

        # Футболки, майки
        url(r'/kids_tshorts/$', catalog_list, name='kids_tshorts'),
        url(r'/kids_tshorts/(\w+)/$', catalog_detail, name='kids_tshorts'),

        # Шортики
        # url(r'/kids_shorts/$', catalog_list, name='kids_shorts'),
        # url(r'/kids_shorts/(\w+)/$', catalog_detail, name='kids_shorts'),

        # Купальники
        url(r'/kids_swimsuit/$', catalog_list, name='kids_swimsuit'),
        url(r'/kids_swimsuit/(\w+)/$', catalog_detail, name='kids_swimsuit'),

        # Трусы
        # url(r'/kids_briefs/$', catalog_list, name='kids_briefs'),
        # url(r'/kids_briefs/(\w+)/$', catalog_detail, name='kids_briefs'),

        # Шапки, шарфы
        url(r'/kids_hats/$', catalog_list, name='kids_hats'),
        url(r'/kids_hats/(\w+)/$', catalog_detail, name='kids_hats'),

        # Кеды
        # url(r'/kids_shoes/$', catalog_list, name='kids_shoes'),
        # url(r'/kids_shoes/(\w+)/$', catalog_detail, name='kids_shoes'),

        url(r'/(\w+)/(?P<item_model_slug>\w+)/quick/$', catalog_item_quick_view, name='catalog_item_quick_view'),

        url(r'/(\w+)/(\w+)/$', catalog_detail, name='catalog_detail'),

        url(r'/$', catalog_list, name='catalog_all'),

    ))),
)