# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.items.models import (SizeType, SizeVal, Specification,
                              Item, Color, SpecificationImages,
                              Category, Brand, Season, ColorPaint)
from rvt.django_ffiler.admin import ImageInlineAdmin
from nested_inlines.admin import NestedModelAdmin, NestedStackedInline, NestedTabularInline
from sorl.thumbnail import ImageField
from rvt.img_sorl import AdminImageWidget
from relatedwidget import RelatedWidgetWrapperBase
from rvt.items.forms import ChangeCategoryForm
from django.http import HttpResponseRedirect
from django.shortcuts import render

class SpecificationImagesInLine(NestedStackedInline):
    model = SpecificationImages
    extra = 0

class SpecificationAdmin(admin.ModelAdmin):
    filter_horizontal = ('size_list',)
    inlines = (SpecificationImagesInLine, )

class SpecificationInLine(RelatedWidgetWrapperBase, NestedStackedInline):
    model = Specification
    inlines = [SpecificationImagesInLine, ]
    filter_horizontal = ('size_list',)
    extra = 0

def move_to_category(modeladmin, request, queryset):
       form = None

       if 'apply' in request.POST:
           form = ChangeCategoryForm(request.POST)

           if form.is_valid():
               category = form.cleaned_data['category']

               count = 0
               for item in queryset:
                   item.category = category
                   item.save()
                   count += 1

               modeladmin.message_user(request, "Категория %s применена к %d товарам." % (category, count))
               return HttpResponseRedirect(request.get_full_path())

       if not form:
           form = ChangeCategoryForm(initial={'_selected_action': request.POST.getlist(admin.ACTION_CHECKBOX_NAME)})

       return render(request, 'move_to_category.html', {'items': queryset,'form': form, 'title':u'Изменение категории'})

move_to_category.short_description = u"Изменить категорию"

class ItemAdmin(NestedModelAdmin):
    list_display = ['get_desc', 'image_preview', 'name', 'category', 'brand', 'item_model', 'sex', 'season', 'price', 'date_add', ]
    list_display_links = ['name', 'brand', 'item_model', ]
    search_fields = ['brand__name', 'item_model', 'name']
    list_filter = ['category', ]
    actions = [move_to_category,]

    inlines = (SpecificationInLine, )

admin.site.register(SizeType)
admin.site.register(SizeVal)
admin.site.register(Specification, SpecificationAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Color)
admin.site.register(SpecificationImages)
admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Season)
admin.site.register(ColorPaint)