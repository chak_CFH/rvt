# -*- coding: utf-8 -*-
from django import forms
from rvt.items.models import Brand, SizeVal, Color, Season, Category

COLOR = [(color.id, color) for color in Color.objects.all()]
SIZE = [(size, size) for size in SizeVal.objects.all()]
BRAND = [(brand, brand) for brand in Brand.objects.all()]
# SEASON = [(season, season) for season in Season.objects.all()]
class FilterForm(forms.Form):
    ''' Общая форма фильтрации. '''
    price_min = forms.CharField(
        label=u'от',
        required=False,
        widget=forms.TextInput(attrs={'value': 0})
    )
    price_max = forms.CharField(
        label=u'до',
        required=False,
        widget=forms.TextInput(attrs={'value': 0})
    )
    brand = forms.MultipleChoiceField(
        label=u'Брэнд',
        widget=forms.CheckboxSelectMultiple,
        choices=BRAND,
        required=False,
    )
    color = forms.MultipleChoiceField(
        label=u'Цвет',
        widget=forms.CheckboxSelectMultiple,
        choices=COLOR,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        # self.fields['price_min'].widget = forms.HiddenInput()
        # self.fields['price_max'].widget = forms.HiddenInput()

        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'some-class other-class'

    def as_div(self):
        return self._html_output(
            normal_row = u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row = u'<div class="error">%s</div>',
            row_ender = '</div>',
            help_text_html = u'<div class="hefp-text">%s</div>',
            errors_on_separate_row = False)

class GraffityFilterForm(FilterForm):
    SIZE = [(size, size) for size in SizeVal.objects.filter(size_type__title=u'Краска')]

    color = forms.MultipleChoiceField(
        label=u'Цвет',
        widget=forms.CheckboxSelectMultiple,
        choices=COLOR,
        required=False,
    )
    size = forms.MultipleChoiceField(
        label=u'Размер',
        widget=forms.CheckboxSelectMultiple,
        choices=SIZE,
        required=False
    )

class ClothesFilterForm(FilterForm):
    ''' Форма фильтрации для одежды '''
    SIZE = [(size, size) for size in SizeVal.objects.filter(size_type__title=u'Одежда')]

    brand = forms.MultipleChoiceField(
        label=u'Брэнд',
        widget=forms.CheckboxSelectMultiple,
        choices=BRAND,
        required=False,
    )
    size = forms.MultipleChoiceField(
        label=u'Размер',
        widget=forms.CheckboxSelectMultiple,
        choices=SIZE,
        required=False
    )
    season = forms.ModelChoiceField(
        label=u'Сезон',
        queryset=Season.objects.all(),
        required=False,
    )

class ShoesFilterForm(FilterForm):
    ''' Форма фильтрации для одежды '''
    SIZE = [(size, size) for size in SizeVal.objects.filter(size_type__title=u'Обувь')]

    brand = forms.MultipleChoiceField(
        label=u'Брэнд',
        widget=forms.CheckboxSelectMultiple,
        choices=BRAND,
        required=False,
    )
    size = forms.MultipleChoiceField(
        label=u'Размер',
        widget=forms.CheckboxSelectMultiple,
        choices=SIZE,
        required=False
    )

class EquipmentFilterForm(FilterForm):
    ''' Форма фильтрации по оборудованию '''
    SIZE = [(size, size) for size in SizeVal.objects.filter(size_type__title=u'Оборудование')]

    brand = forms.MultipleChoiceField(
        label=u'Брэнд',
        widget=forms.CheckboxSelectMultiple,
        choices=BRAND,
        required=False,
    )
    season = forms.ModelChoiceField(
        queryset=Season.objects.all(),
        required=False,
    )
    color = forms.MultipleChoiceField(
        label=u'Цвет',
        widget=forms.CheckboxSelectMultiple,
        choices=COLOR,
        required=False,
    )
    size = forms.MultipleChoiceField(
        label=u'Размер',
        widget=forms.CheckboxSelectMultiple,
        choices=SIZE,
        required=False,
    )

class AccessoriesFilterForm(FilterForm):
    ''' Форма фильтрации по аксесуарам '''
    brand = forms.MultipleChoiceField(
        label=u'Брэнд',
        widget=forms.CheckboxSelectMultiple,
        choices=BRAND,
        required=False,
    )
    color = forms.MultipleChoiceField(
        label=u'Цвет',
        widget=forms.CheckboxSelectMultiple,
        choices=COLOR,
        required=False,
    )

class OpticsFilterForm(AccessoriesFilterForm):
    ''' Форма фильтрации по оптике. '''
    pass

class ChangeCategoryForm(forms.Form):
    ''' Форма-экшн в админке для переноса нескольких товаров в другую категорию. '''
    _selected_action = forms.CharField(widget=forms.MultipleHiddenInput)
    category = forms.ModelChoiceField(queryset=Category.objects.all(), label=u'Категория')