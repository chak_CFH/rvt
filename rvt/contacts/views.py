# -*- coding: utf-8 -*-
from django.shortcuts import render
from rvt.contacts.models import ContactsTeam, ContactsMap, Contacts


def contacts(request):
    contactsteam = ContactsTeam.objects.all()
    cntactsmap  = ContactsMap.objects.all()
    contacts = Contacts.objects.all()[:1].get()
    return render(request, 'contacts.jade', {
        'contactsteam': contactsteam,
        'cntactsmap': cntactsmap,
        'contacts': contacts,
        'breadcrumbs': [
            {'url':'/','title':'Главная'},
            {'url':'/contacts','title':'Контакты'}
        ]
    })
