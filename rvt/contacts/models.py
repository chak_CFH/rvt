# -*- coding: utf-8 -*-
import re
from django.db import models
from tinymce.models import HTMLField
from sorl.thumbnail import ImageField

class ContactsTeam(models.Model):
    ''' Контакты. Команда магазина '''
    image = ImageField(u'Фото', upload_to='how_to_find_us/', blank=True, null=True)
    name = models.CharField(u'Имя', max_length=30, blank=True, null=True)
    surname = models.CharField(u'Фамилия', max_length=60, blank=True, null=True)
    position = models.CharField(u'Должность', max_length=255, blank=True, null=True)
    phone = models.CharField(u'Тел.', max_length=30, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)

    class Meta:
        verbose_name = u'Контакты. Наша команда'
        verbose_name_plural = u'Контакты. Наша команда'

    def __unicode__(self):
        return '%s %s' % (self.name, self.surname)

    def get_full_name(self):
        return '%s %s' % (self.name, self.surname)

    def image_preview(self):
        if self.image:
            return '<img src="%s" width="100">' % self.image.url
        else:
            return '(none)'
    image_preview.short_description = u'Изображение'
    image_preview.allow_tags = True

    def save(self, *args, **kwargs):
        self.phone = re.sub(u"[^0-9\s +.()\-]", "", self.phone)
        super(ContactsTeam, self).save(*args, **kwargs)

class ContactsMap(models.Model):
    ''' Контакты. Как нас найти. '''
    contact_map = HTMLField(u'Как нас найти')

    class Meta:
        verbose_name = u'Контакты. Как нас найти(Карта)'
        verbose_name_plural = u'Контакты. Как нас найти(Карта)'

    def __unicode__(self):
        return u'Как нас найти'

class Contacts(models.Model):
    address = models.CharField(u'Заходите на чай', max_length=225)
    phone = models.CharField(u'Звоните, когда скучно', max_length=30)
    workdaystime = models.CharField(u'Ежедневно', max_length=50)

    def __unicode__(self):
        return u'%s, %s, %s' % (
            self.address,
            self.phone,
            self.workdaystime,
        )