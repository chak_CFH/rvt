# -*- coding: utf-8 -*-
from django.contrib import admin
from rvt.contacts.models import ContactsTeam, ContactsMap, Contacts
from sorl.thumbnail import ImageField
from rvt.img_sorl import AdminImageWidget


class ContacsTeamAdmin(admin.ModelAdmin):
	list_display = ('image_preview', 'name', 'surname', 'position', 'phone', 'email')
	formfield_overrides = {
		ImageField: {'widget': AdminImageWidget},
	}

admin.site.register(ContactsTeam, ContacsTeamAdmin)
admin.site.register(ContactsMap)
admin.site.register(Contacts)
